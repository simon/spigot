#!/usr/bin/perl

# Evil Perlery here to get some red text in one of the code examples,
# which Halibut doesn't support natively.
while (<STDIN>) {
    s{<em>(.* \(\d+\^-.*)</em>}{<span style="color: red">$1</span>};
    print;
}
