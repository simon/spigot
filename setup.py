#!/usr/bin/env python3

# Python distutils script for the spigot Python module.

import os
import glob
from distutils.core import setup, Extension

# Fetch the list of spigot source files from CMakeLists.txt so that we
# don't have to have a second copy of it in here to get out of date.
spigot_modules = []
with open(os.path.join(os.path.dirname(__file__), "CMakeLists.txt")) as cml:
    in_definition = False
    for line in iter(cml.readline, ''):
        if line.startswith("set(SPIGOT_SHARED_MODULES"):
            in_definition = True
        if in_definition:
            for word in line.rstrip("\n").rstrip("\r").rstrip(")").split(" "):
                if word.endswith(".cpp"):
                    spigot_modules.append(word)
        if ")" in line:
            in_definition = False

# Add files that are only used by the Python module
spigot_modules.append("python-module/internal.cpp")

setup(name="spigot-py",
      version="0.0.dev0",
      description="Exact real calculation by spigot algorithm",
      author="Simon Tatham",
      author_email="anakin@pobox.com",
      packages=["spigot"],
      package_dir={'spigot':'python-module'},
      ext_modules=[Extension("spigot._internal",
                             spigot_modules,
                             include_dirs=["."],
                             libraries=["gmp"],
                             extra_compile_args=['-std=c++14',
                                                 '-Wno-write-strings'],
                             define_macros=[("HAVE_LIBGMP",None)],
      )])
