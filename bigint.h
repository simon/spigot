/* -*- c++ -*-
 * Switch header which includes an appropriate bi_* header to get one
 * or other implementation of bigints.
 */

#if HAVE_LIBGMP
#include "bi_gmp.h"
#else
#include "bi_internal.h"
#endif
