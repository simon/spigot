/* -*- c++ -*-
 * cfracout.h: header for cfracout.cpp.
 */

#include <memory>
using std::unique_ptr;

/*
 * Functions that construct OutputGenerators for the various
 * continued-fraction based output modes.
 */
unique_ptr<OutputGenerator> cfrac_output(
    Spigot spig, bool oneline, bool has_digitlimit, int digitlimit);
unique_ptr<OutputGenerator> convergents_output(
    Spigot spig, bool has_digitlimit, int digitlimit);
unique_ptr<OutputGenerator> rational_output(
    Spigot spig, bool has_digitlimit);
