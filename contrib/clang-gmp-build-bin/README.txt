This bin directory can be used to cross-build (on Linux) a GMP static
library for Windows suitable to link spigot against.

Instructions:
 - unpack the GMP original source tarball
 - ensure the LLVM tools and 'ar' are on your PATH (in particular:
   clang, llvm-lib, lld-link), and that INCLUDE and LIB are set up
   appropriately for cross-compiling, e.g. via msvc-extract
 - in a build directory, run
      configure-gmp $srcdir --prefix=$instdir
   where $srcdir is the source directory created by unpacking the GMP
   sources, and $instdir is the directory under which you want to make
   include and lib subdirectories.
 - in that build directory, run 'make'
 - in that build directory, run 'make install'.
