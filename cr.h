/* -*- c++ -*-
 * cr.h: Coroutine mechanics for C++.
 *
 * For an explanation of the technique, see
 * 
 *   http://chiark.greenend.org.uk/~sgtatham/coroutines.html
 */

#include <assert.h>
#include <stdlib.h>

#define crBegin switch (crState) { case -1:;
#define crEnd } assert(!"Should never get here"); abort();
#define crReturn(ret) do { \
    crState = __LINE__; return ret; case __LINE__:; \
} while (0)
#define crReturnV do { \
    crState = __LINE__; return; case __LINE__:; \
} while (0)
