/*
 * noexcept.cpp: a null implementation of spigot_check_exception() for
 * use in the command-line spigot program.
 */

#include <stdlib.h>

#include "spigot.h"

void spigot_check_exception()
{
}
