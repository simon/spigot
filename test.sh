#!/bin/bash 

SPIGOT=spigot
PREFIX=()

nargs=0
processing_opts=y
while test $# -gt 0; do
    case "$processing_opts:$1" in
        y:--) processing_opts=n;;
        y:--help) cat <<EOF
usage:   test.sh [ options ] [ <spigot-program> ]
options: --valgrind=LOG  run spigot under valgrind, sending logs to LOG
         --help          print this text
EOF
            exit 0;;
        y:--valgrind | y:--valgrind=*)
            case "$1" in
                --valgrind=*) optval="${1##*=}";;
                *) if test $# -lt 2; then
                    echo "test.sh: option '$1' expects an argument" >&2
                    exit 1
                    fi
                    optval="$2"; shift;;
            esac
            exec 3>"$optval"
            PREFIX=(valgrind --log-fd=3);;
        # For options taking a value, follow the pattern of --valgrind
        # above. For options not taking a value, just add a case of
        # the form
        #     y:--option) do stuff;;
        y:-*) echo "test.sh: unknown option '$1'" >&2; exit 1;;
        ?:*)
            case $nargs in
                0) SPIGOT="$1";;
                *) echo "test.sh: unexpected extra argument '$1'" >&2; exit 1;;
            esac
            nargs=$((nargs+1));;
    esac
    shift
done

# We will run spigot in a temporary directory, so we need to know a
# pathname for it that is not cwd-relative. Search for it on PATH if
# it's a bare name, and then try to canonicalise via realpath or
# readlink if either is available. Failing that, the user will just
# have to specify an absolute path on input.
SPIGOT="$(type -P "$SPIGOT")"
if [[ "$SPIGOT" =~ ^/ ]]; then
    : # pathname is already ok
elif test -n "$(type -t realpath)"; then
    SPIGOT="$(realpath "$SPIGOT")"
elif test -n "$(type -t readlink)"; then
    SPIGOT="$(readlink -f "$SPIGOT")"
else
    echo "test.sh: unable to canonicalise spigot path" \
        "'$1'; please give an absolute path" >&2
    exit 1
fi

pass=0
fail=0

testcase() {
    local line=${BASH_LINENO[-2]}
    local expectedstatus=0
    local expected="$1"
    shift
    case "$1" in exit:*) expectedstatus=${1#exit:}; shift;; esac
    local actual
    actual=$("${PREFIX[@]}" "$SPIGOT" "$@" 2>/dev/null)
    local actualstatus=$?
    actual=$(echo "$actual" | tr -d \\r)
    if test "z$actual" != "z$expected" -o \
        "z$actualstatus" != "z$expectedstatus"; then
        echo -n "Failed test at line $line: spigot"
        for arg in "$@"; do
            case $arg in
                *[!0-9a-zA-Z\-]*) echo -n " '${arg/'/'\\''/}'";;
                *) echo -n " $arg";;
            esac
        done
        echo
        echo "Expected: status $expectedstatus, output '$expected'"
        echo "  Actual: status $actualstatus, output '$actual'"
        echo
        fail=$((fail+1))
    else
        pass=$((pass+1))
    fi
}

# Start by dumping the program's version and combing it for knowledge
# of which parts of this test suite we can run.
echo ---- general info
echo "Program under test:"
echo "| $SPIGOT"
echo "Version dump:"
HAVE_FDS=false
while IFS= read versionline; do
    echo "| $versionline"
    case "$versionline" in
        '  fd literals'*': supported') HAVE_FDS=true
    esac
done < <("$SPIGOT" --version)

echo ---- core tests

oldpwd="$(pwd)"
if tmpdir="$(mktemp -d)" && cd "$tmpdir"; then
    :
else
    echo "Unable to set up a temporary directory" >&2
    exit 1
fi

# Simple tests of rationals
testcase  '4b000000.8' -S -d40 -- '8388608.5'
testcase  '-2'    -d70 -- '-2'
testcase  '0'    -d70 -- '0'
testcase '0.2'    -d70 -- '1/5'

# IEEEish representations of pi
testcase '4248.7ed5110b46' -H -d40 -- 'pi'
testcase '40490fda.a22168c234' -S -d40 -- 'pi'
testcase '400921fb54442d18.469898cc51' -D -d40 -- 'pi'
testcase '4000921fb54442d18469898cc51701b8.39a252049c' -Q -d40 -- 'pi'

# A quick check that baseN: input is working
testcase '48413765' base36:STOAT
testcase '3.1415920257568359375' base2:11.0010010000111111011
testcase '1.3610416955035047827413403586668340818282' -d40 base7:1.234560123456

# Maths functions and arithmetic
testcase     '3.1415926535897932384626433832795028841971693993751058209749445923078164'    -d70 -- 'pi'
testcase    '-3.1415926535897932384626433832795028841971693993751058209749445923078164'    -d70 -- '-pi'
testcase     '2.7182818284590452353602874713526624977572470936999595749669676277240766'    -d70 -- 'e'
testcase     '1.6180339887498948482045868343656381177203091798057628621354486227052604'    -d70 -- 'phi'
testcase     '1.2020569031595942853997381615114499907649862923404988817922715553418382'    -d70 -- 'apery'
testcase     '0.9159655941772190150546035149323841107741493742816721342664981196217630'    -d70 -- 'catalan'
testcase     '5.8598744820488384738229308546321653819544164930750653959419122200318930'    -d70 -- 'pi+e'
testcase     '0.4233108251307480031023559119268403864399223056751462460079769645837397'    -d70 -- 'pi-e'
testcase    '-0.4233108251307480031023559119268403864399223056751462460079769645837397'    -d70 -- 'e-pi'
testcase     '1.1557273497909217179100931833126962991208510231644158204997065353272886'    -d70 -- 'pi/e'
testcase     '8.5397342226735670654635508695465744950348885357651149618796011301792286'    -d70 -- 'pi*e'
testcase    '-1.1557273497909217179100931833126962991208510231644158204997065353272886'    -d70 -- 'pi/-e'
testcase     '1.7724538509055160272981674833411451827975494561223871282138077898529112'    -d70 -- 'sqrt(pi)'
testcase  '1772.4538509055160272981674833411451827975494561223871282138077898529112845'    -d70 -- 'sqrt(pi*1000000)'
testcase  '2506.6282746310005024157652848110452530069867406099383166299235763422936546'    -d70 -- 'sqrt(pi*2000000)'
testcase  '3544.9077018110320545963349666822903655950989122447742564276155797058225691'    -d70 -- 'sqrt(pi*4000000)'
testcase     '0.4012319619908143541857543436532949583238702611292440683194415381168718'    -d70 -- 'tan(1e100)'
testcase    '23.1406926327792690057290863679485473802661062426002119934450464095243423'    -d70 -- 'exp(pi)'
testcase    '22.4591577183610454734271522045437350275893151339966922492030025540669260'    -d70 -- 'pi^e'
testcase    '-1.1902899496825317329277337748293183376011789860294520729111666738297077'    -d70 -- 'atan(-2.5)'
testcase    '-1.1071487177940905030170654601785370400700476454014326466765392074337103'    -d70 -- 'atan(-2)'
testcase    '-0.8329812666744317054176935618363612385158513444371084208534231225032752'    -d70 -- 'atan(-1.1)'
testcase    '-0.7853981633974483096156608458198757210492923498437764552437361480769541'    -d70 -- 'atan(-1)'
testcase    '-0.7328151017865065916407920727342802519857556793582560863105069319282124'    -d70 -- 'atan(-0.9)'
testcase    '-0.4636476090008061162142562314612144020285370542861202638109330887201978'    -d70 -- 'atan(-0.5)'
testcase    '-0.0996686524911620273784461198780205902432783225043146480155087768100277'    -d70 -- 'atan(-0.1)'
testcase     '0.0996686524911620273784461198780205902432783225043146480155087768100277'    -d70 -- 'atan(+0.1)'
testcase     '0.4636476090008061162142562314612144020285370542861202638109330887201978'    -d70 -- 'atan(+0.5)'
testcase     '0.7328151017865065916407920727342802519857556793582560863105069319282124'    -d70 -- 'atan(+0.9)'
testcase     '0.7853981633974483096156608458198757210492923498437764552437361480769541'    -d70 -- 'atan(+1)'
testcase     '0.8329812666744317054176935618363612385158513444371084208534231225032752'    -d70 -- 'atan(+1.1)'
testcase     '1.1071487177940905030170654601785370400700476454014326466765392074337103'    -d70 -- 'atan(+2)'
testcase     '1.1902899496825317329277337748293183376011789860294520729111666738297077'    -d70 -- 'atan(+2.5)'
testcase     '1.3956124250860895286281253196025868375979065151994069826175167060317390'    -d70 -- 'exp(1/3)'
testcase     '1.0100501670841680575421654569028600338073622015242925151644040312543741'    -d70 -- 'exp(1/100)'
testcase     '0.7165313105737892504256040969253796674531120598214791571408702071273040'    -d70 -- 'exp(-1/3)'
testcase     '0.9900498337491680535739059771800365577720790812538374668838787452931477'    -d70 -- 'exp(-1/100)'
testcase     '0.3271946967961522441733440852676206060643014068937597915900562770705763'    -d70 -- 'sin(1/3)'
testcase     '0.0099998333341666646825424382690997290389643853601691510338791124794097'    -d70 -- 'sin(1/100)'
testcase    '-0.3271946967961522441733440852676206060643014068937597915900562770705763'    -d70 -- 'sin(-1/3)'
testcase    '-0.0099998333341666646825424382690997290389643853601691510338791124794097'    -d70 -- 'sin(-1/100)'
testcase     '0.9449569463147376643882840076758806078458526995651407376776457337500995'    -d70 -- 'cos(1/3)'
testcase     '0.9999500004166652777802579337522066732124705839802771111222757686464600'    -d70 -- 'cos(1/100)'
testcase     '0.9449569463147376643882840076758806078458526995651407376776457337500995'    -d70 -- 'cos(-1/3)'
testcase     '0.9999500004166652777802579337522066732124705839802771111222757686464600'    -d70 -- 'cos(-1/100)'
testcase     '1.2572741156691850593845221141104482939061663100396581735341816226271627'    -d70 -- 'pi^(1/5)'
testcase     '1.0986122886681096913952452369225257046474905578227494517346943336374942'    -d70 -- 'log(3)'
testcase   '363.7393755555634901440799933696556380278239210628872747276794488767759444'    -d70 -- 'lgamma(101)'
testcase   '363.7393755555634901440799933696556380278239210628872747276794488767759444'    -d70 -- 'log(93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000)' # 100! == gamma(101)
testcase    '-1.1197695149986341866866770558453996158951621864033028823756818639144375'    -d70 -- 'asin(-0.9)'
testcase    '-0.9272952180016122324285124629224288040570741085722405276218661774403957'    -d70 -- 'asin(-0.8)'
testcase    '-0.7753974966107530637403533527149871135557887386411619935977199637327202'    -d70 -- 'asin(-0.7)'
testcase    '-0.6435011087932843868028092287173226380415105911153123828656061187135124'    -d70 -- 'asin(-0.6)'
testcase    '-0.5235987755982988730771072305465838140328615665625176368291574320513027'    -d70 -- 'asin(-0.5)'
testcase    '-0.4115168460674880193847378976173356048557011351270258517839467807000952'    -d70 -- 'asin(-0.4)'
testcase    '-0.3046926540153975079720029612275291669545600317067763873929779487464729'    -d70 -- 'asin(-0.3)'
testcase    '-0.2013579207903307914551255522176234102400380814022283862572512434556093'    -d70 -- 'asin(-0.2)'
testcase    '-0.1001674211615597963455231794526933185686759722296295413910238550364026'    -d70 -- 'asin(-0.1)'
testcase     '0.1001674211615597963455231794526933185686759722296295413910238550364026'    -d70 -- 'asin(0.1)'
testcase     '0.2013579207903307914551255522176234102400380814022283862572512434556093'    -d70 -- 'asin(0.2)'
testcase     '0.3046926540153975079720029612275291669545600317067763873929779487464729'    -d70 -- 'asin(0.3)'
testcase     '0.4115168460674880193847378976173356048557011351270258517839467807000952'    -d70 -- 'asin(0.4)'
testcase     '0.5235987755982988730771072305465838140328615665625176368291574320513027'    -d70 -- 'asin(0.5)'
testcase     '0.6435011087932843868028092287173226380415105911153123828656061187135124'    -d70 -- 'asin(0.6)'
testcase     '0.7753974966107530637403533527149871135557887386411619935977199637327202'    -d70 -- 'asin(0.7)'
testcase     '0.9272952180016122324285124629224288040570741085722405276218661774403957'    -d70 -- 'asin(0.8)'
testcase     '1.1197695149986341866866770558453996158951621864033028823756818639144375'    -d70 -- 'asin(0.9)'
testcase     '0.6435011087932843868028092287173226380415105911153123828656061187135124'    -d70 -- 'atan2(3,4)'
testcase    '-0.3805063771123648863035879168104331044974057136581008375763056223242004'    -d70 -- 'atan2(-2,5)'
testcase     '1.7126933813990605420441733087423045251063664584160169828656023255173498'    -d70 -- 'atan2(7,-1)'
testcase    '-2.5535900500422256872170323026544174565954621533191814672488973844718962'    -d70 -- 'atan2(-6,-9)'
testcase     '0'                                                                           -d70 -- 'atan2(0,1)'
testcase     '3.1415926535897932384626433832795028841971693993751058209749445923078164'    -d70 -- 'atan2(0,-1)'
testcase     '1.5707963267948966192313216916397514420985846996875529104874722961539082'    -d70 -- 'atan2(1,0)'
testcase    '-1.5707963267948966192313216916397514420985846996875529104874722961539082'    -d70 -- 'atan2(-1,0)'
testcase     '0.8414709848078965066525023216302989996225630607983710656727517099919104'    -d70 -- 'sinc(1)'
testcase     '1'                                                                           -d70 -- 'sinc(0)'
testcase     '0.8414709848078965066525023216302989996225630607983710656727517099919104'    -d70 -- 'sinc(1)'
testcase     '0.9588510772084060005465758704311427761636067358812013503772332262510700'    -d70 -- 'sinc(1/2)'
testcase     '1'                                                                           -d70 -- 'sinc(0)'
testcase     '0.9588510772084060005465758704311427761636067358812013503772332262510700'    -d70 -- 'sinc(-1/2)'
testcase     '0.8414709848078965066525023216302989996225630607983710656727517099919104'    -d70 -- 'sinc(-1)'
testcase     '0'                                                                           -d70 -- 'sincn(1)'
testcase     '0.6366197723675813430755350534900574481378385829618257949906693762355871'    -d70 -- 'sincn(1/2)'
testcase     '1'                                                                           -d70 -- 'sincn(0)'
testcase     '0.6366197723675813430755350534900574481378385829618257949906693762355871'    -d70 -- 'sincn(-1/2)'
testcase     '0'                                                                           -d70 -- 'sincn(-1)'
testcase     '5'                                                                           -d70 -- 'hypot(3,4)'
testcase     '13'                                                                          -d70 -- 'hypot(3,4,12)'
testcase     '85'                                                                          -d70 -- 'hypot(3,4,12,84)'
testcase     '157'                                                                         -d70 -- 'hypot(3,4,12,84,132)'
testcase     '5.3851648071345040312507104915403295562951201616447888376803886700166459'    -d70 -- 'hypot(-2,5)'
testcase     '7.0710678118654752440084436210484903928483593768847403658833986899536623'    -d70 -- 'hypot(7,-1)'
testcase    '10.8166538263919678793576638024114878387538897215357386381313591686815008'    -d70 -- 'hypot(-6,-9)'
testcase     '1'                                                                           -d70 -- 'hypot(0,1)'
testcase     '1'                                                                           -d70 -- 'hypot(0,-1)'
testcase     '1'                                                                           -d70 -- 'hypot(1,0)'
testcase     '1'                                                                           -d70 -- 'hypot(-1,0)'
testcase   '-74.2032105777887589770094719960645655996194090044258169806612697896696906'    -d70 -- 'sinh(-5)'
testcase    '-0.1001667500198440258237293835219050235149209168785588833683029861925940'    -d70 -- 'sinh(-.1)'
testcase     '0.1001667500198440258237293835219050235149209168785588833683029861925940'    -d70 -- 'sinh(.1)'
testcase    '74.2032105777887589770094719960645655996194090044258169806612697896696906'    -d70 -- 'sinh(5)'
testcase    '74.2099485247878444441061080444877140238682585894531720660915753212423741'    -d70 -- 'cosh(-5)'
testcase     '1.0050041680558035989879784429683416447096262778589598354245603032483739'    -d70 -- 'cosh(-.1)'
testcase     '1.0050041680558035989879784429683416447096262778589598354245603032483739'    -d70 -- 'cosh(.1)'
testcase    '74.2099485247878444441061080444877140238682585894531720660915753212423741'    -d70 -- 'cosh(5)'
testcase    '-0.9999092042625951312109904475344730210898126159905478627364288722625610'    -d70 -- 'tanh(-5)'
testcase    '-0.0996679946249558171183050836783521835389620957767344369304764385439917'    -d70 -- 'tanh(-.1)'
testcase     '0.0996679946249558171183050836783521835389620957767344369304764385439917'    -d70 -- 'tanh(.1)'
testcase     '0.9999092042625951312109904475344730210898126159905478627364288722625610'    -d70 -- 'tanh(5)'
testcase    '-2.3124383412727526202535623413644143836582450726465592371672289900991325'    -d70 -- 'asinh(-5)'
testcase    '-0.0998340788992075633273031247047694432677129117088250107423826956515917'    -d70 -- 'asinh(-.1)'
testcase     '0.0998340788992075633273031247047694432677129117088250107423826956515917'    -d70 -- 'asinh(.1)'
testcase     '2.3124383412727526202535623413644143836582450726465592371672289900991325'    -d70 -- 'asinh(5)'
testcase     '0.4435682543851151891329110663524980866490011660999754638934209558076688'    -d70 -- 'acosh(1.1)'
testcase     '2.2924316695611776878007873113480154316218682400157102476050164448313478'    -d70 -- 'acosh(5)'
testcase     '4.6050701709847571594505725515130629155479434834765415773570077019244073'    -d70 -- 'acosh(50)'
testcase    '-1.4722194895832202300045137159439267686186896306495644092689801182046463'    -d70 -- 'atanh(-.9)'
testcase    '-0.1003353477310755806357265520600389452633628691459591358745895209277925'    -d70 -- 'atanh(-.1)'
testcase     '0.1003353477310755806357265520600389452633628691459591358745895209277925'    -d70 -- 'atanh(.1)'
testcase     '1.4722194895832202300045137159439267686186896306495644092689801182046463'    -d70 -- 'atanh(.9)'
testcase '-1353.2868196796119012120782386750901706773155970518514189830847088458916602'    -d70 -- 'cbrt(-2478389475)'
testcase  '1353.2868196796119012120782386750901706773155970518514189830847088458916602'    -d70 -- 'cbrt(2478389475)'
testcase    '-1.2599210498948731647672106072782283505702514647015079800819751121552996'    -d70 -- 'cbrt(-2)'
testcase     '1.2599210498948731647672106072782283505702514647015079800819751121552996'    -d70 -- 'cbrt(2)'
testcase     '1.4645918875615232630201425272637903917385968556279371743572559371383936'    -d70 -- 'cbrt(pi)'
testcase    '-1.4645918875615232630201425272637903917385968556279371743572559371383936'    -d70 -- 'cbrt(-pi)'
testcase   '146.4591887561523263020142527263790391738596855627937174357255937138393649'    -d70 -- 'cbrt(pi*1000000)'
testcase   '184.5270148644028419096803879588988026780036266215951662725508973334767943'    -d70 -- 'cbrt(pi*2000000)'
testcase   '232.4894703019252951141799628955674133718676185523674068779515445375023295'    -d70 -- 'cbrt(pi*4000000)'
testcase   '292.9183775123046526040285054527580783477193711255874348714511874276787299'    -d70 -- 'cbrt(pi*8000000)'
testcase     '0.1415926535897932384626433832795028841971693993751058209749445923078164'    -d70 -- 'frac(pi)'
testcase     '0.8584073464102067615373566167204971158028306006248941790250554076921835'    -d70 -- 'frac(-pi)'
testcase     '0.1013211836423377714438794632097276389043587746722465488456090318941731'    -d70 -- 'pi^-2'

# Check the rational^integer special-case code for pow()
testcase '2048' 2^11
testcase '4096' 2^12
testcase '0.00048828125' 2^-11
testcase '0.000244140625' 2^-12
testcase '2.7777777777777777777777777777777777777777' -d40 -- '(-3/5)^-2'
testcase '-1.6666666666666666666666666666666666666666' -d40 -- '(-3/5)^-1'
testcase '1' -d40 -- '(-3/5)^0'
testcase '-0.6' -d40 -- '(-3/5)^1'
testcase '0.36' -d40 -- '(-3/5)^2'
testcase '8' 'exp2(3)'
testcase '0.01' 'exp10(-2)'

# Check IEEE 754 tie-breaking in rem
testcase '-0.5' -- '-2.5 rem 1'
testcase  '0.5' -- '-1.5 rem 1'
testcase '-0.5' -- '-0.5 rem 1'
testcase  '0.5' -- '+0.5 rem 1'
testcase '-0.5' -- '+1.5 rem 1'
testcase  '0.5' -- '+2.5 rem 1'

# Tests of all rounding modes and no rounding mode
testcase '4c000000'   -S           --  '33554432'
testcase '4c000000.4' -S           --  '33554433'
testcase '4c000000.8' -S           --  '33554434'
testcase '4c000000.c' -S           --  '33554435'
testcase '4c000001'   -S           --  '33554436'
testcase '4c000001.4' -S           --  '33554437'
testcase '4c000001.8' -S           --  '33554438'
testcase '4c000001.c' -S           --  '33554439'
testcase '4c000002'   -S           --  '33554440'
testcase 'cc000000'   -S           -- '-33554432'
testcase 'cc000000.4' -S           -- '-33554433'
testcase 'cc000000.8' -S           -- '-33554434'
testcase 'cc000000.c' -S           -- '-33554435'
testcase 'cc000001'   -S           -- '-33554436'
testcase 'cc000001.4' -S           -- '-33554437'
testcase 'cc000001.8' -S           -- '-33554438'
testcase 'cc000001.c' -S           -- '-33554439'
testcase 'cc000002'   -S           -- '-33554440'
testcase '4c000000'   -S -d0 --rz  --  '33554432'
testcase '4c000000'   -S -d0 --rz  --  '33554433'
testcase '4c000000'   -S -d0 --rz  --  '33554434'
testcase '4c000000'   -S -d0 --rz  --  '33554435'
testcase '4c000001'   -S -d0 --rz  --  '33554436'
testcase '4c000001'   -S -d0 --rz  --  '33554437'
testcase '4c000001'   -S -d0 --rz  --  '33554438'
testcase '4c000001'   -S -d0 --rz  --  '33554439'
testcase '4c000002'   -S -d0 --rz  --  '33554440'
testcase 'cc000000'   -S -d0 --rz  -- '-33554432'
testcase 'cc000000'   -S -d0 --rz  -- '-33554433'
testcase 'cc000000'   -S -d0 --rz  -- '-33554434'
testcase 'cc000000'   -S -d0 --rz  -- '-33554435'
testcase 'cc000001'   -S -d0 --rz  -- '-33554436'
testcase 'cc000001'   -S -d0 --rz  -- '-33554437'
testcase 'cc000001'   -S -d0 --rz  -- '-33554438'
testcase 'cc000001'   -S -d0 --rz  -- '-33554439'
testcase 'cc000002'   -S -d0 --rz  -- '-33554440'
testcase '4c000000'   -S -d0 --ri  --  '33554432'
testcase '4c000001'   -S -d0 --ri  --  '33554433'
testcase '4c000001'   -S -d0 --ri  --  '33554434'
testcase '4c000001'   -S -d0 --ri  --  '33554435'
testcase '4c000001'   -S -d0 --ri  --  '33554436'
testcase '4c000002'   -S -d0 --ri  --  '33554437'
testcase '4c000002'   -S -d0 --ri  --  '33554438'
testcase '4c000002'   -S -d0 --ri  --  '33554439'
testcase '4c000002'   -S -d0 --ri  --  '33554440'
testcase 'cc000000'   -S -d0 --ri  -- '-33554432'
testcase 'cc000001'   -S -d0 --ri  -- '-33554433'
testcase 'cc000001'   -S -d0 --ri  -- '-33554434'
testcase 'cc000001'   -S -d0 --ri  -- '-33554435'
testcase 'cc000001'   -S -d0 --ri  -- '-33554436'
testcase 'cc000002'   -S -d0 --ri  -- '-33554437'
testcase 'cc000002'   -S -d0 --ri  -- '-33554438'
testcase 'cc000002'   -S -d0 --ri  -- '-33554439'
testcase 'cc000002'   -S -d0 --ri  -- '-33554440'
testcase '4c000000'   -S -d0 --ru  --  '33554432'
testcase '4c000001'   -S -d0 --ru  --  '33554433'
testcase '4c000001'   -S -d0 --ru  --  '33554434'
testcase '4c000001'   -S -d0 --ru  --  '33554435'
testcase '4c000001'   -S -d0 --ru  --  '33554436'
testcase '4c000002'   -S -d0 --ru  --  '33554437'
testcase '4c000002'   -S -d0 --ru  --  '33554438'
testcase '4c000002'   -S -d0 --ru  --  '33554439'
testcase '4c000002'   -S -d0 --ru  --  '33554440'
testcase 'cc000000'   -S -d0 --ru  -- '-33554432'
testcase 'cc000000'   -S -d0 --ru  -- '-33554433'
testcase 'cc000000'   -S -d0 --ru  -- '-33554434'
testcase 'cc000000'   -S -d0 --ru  -- '-33554435'
testcase 'cc000001'   -S -d0 --ru  -- '-33554436'
testcase 'cc000001'   -S -d0 --ru  -- '-33554437'
testcase 'cc000001'   -S -d0 --ru  -- '-33554438'
testcase 'cc000001'   -S -d0 --ru  -- '-33554439'
testcase 'cc000002'   -S -d0 --ru  -- '-33554440'
testcase '4c000000'   -S -d0 --rd  --  '33554432'
testcase '4c000000'   -S -d0 --rd  --  '33554433'
testcase '4c000000'   -S -d0 --rd  --  '33554434'
testcase '4c000000'   -S -d0 --rd  --  '33554435'
testcase '4c000001'   -S -d0 --rd  --  '33554436'
testcase '4c000001'   -S -d0 --rd  --  '33554437'
testcase '4c000001'   -S -d0 --rd  --  '33554438'
testcase '4c000001'   -S -d0 --rd  --  '33554439'
testcase '4c000002'   -S -d0 --rd  --  '33554440'
testcase 'cc000000'   -S -d0 --rd  -- '-33554432'
testcase 'cc000001'   -S -d0 --rd  -- '-33554433'
testcase 'cc000001'   -S -d0 --rd  -- '-33554434'
testcase 'cc000001'   -S -d0 --rd  -- '-33554435'
testcase 'cc000001'   -S -d0 --rd  -- '-33554436'
testcase 'cc000002'   -S -d0 --rd  -- '-33554437'
testcase 'cc000002'   -S -d0 --rd  -- '-33554438'
testcase 'cc000002'   -S -d0 --rd  -- '-33554439'
testcase 'cc000002'   -S -d0 --rd  -- '-33554440'
testcase '4c000000'   -S -d0 --rne --  '33554432'
testcase '4c000000'   -S -d0 --rne --  '33554433'
testcase '4c000000'   -S -d0 --rne --  '33554434'
testcase '4c000001'   -S -d0 --rne --  '33554435'
testcase '4c000001'   -S -d0 --rne --  '33554436'
testcase '4c000001'   -S -d0 --rne --  '33554437'
testcase '4c000002'   -S -d0 --rne --  '33554438'
testcase '4c000002'   -S -d0 --rne --  '33554439'
testcase '4c000002'   -S -d0 --rne --  '33554440'
testcase 'cc000000'   -S -d0 --rne -- '-33554432'
testcase 'cc000000'   -S -d0 --rne -- '-33554433'
testcase 'cc000000'   -S -d0 --rne -- '-33554434'
testcase 'cc000001'   -S -d0 --rne -- '-33554435'
testcase 'cc000001'   -S -d0 --rne -- '-33554436'
testcase 'cc000001'   -S -d0 --rne -- '-33554437'
testcase 'cc000002'   -S -d0 --rne -- '-33554438'
testcase 'cc000002'   -S -d0 --rne -- '-33554439'
testcase 'cc000002'   -S -d0 --rne -- '-33554440'
testcase '4c000000'   -S -d0 --rno --  '33554432'
testcase '4c000000'   -S -d0 --rno --  '33554433'
testcase '4c000001'   -S -d0 --rno --  '33554434'
testcase '4c000001'   -S -d0 --rno --  '33554435'
testcase '4c000001'   -S -d0 --rno --  '33554436'
testcase '4c000001'   -S -d0 --rno --  '33554437'
testcase '4c000001'   -S -d0 --rno --  '33554438'
testcase '4c000002'   -S -d0 --rno --  '33554439'
testcase '4c000002'   -S -d0 --rno --  '33554440'
testcase 'cc000000'   -S -d0 --rno -- '-33554432'
testcase 'cc000000'   -S -d0 --rno -- '-33554433'
testcase 'cc000001'   -S -d0 --rno -- '-33554434'
testcase 'cc000001'   -S -d0 --rno -- '-33554435'
testcase 'cc000001'   -S -d0 --rno -- '-33554436'
testcase 'cc000001'   -S -d0 --rno -- '-33554437'
testcase 'cc000001'   -S -d0 --rno -- '-33554438'
testcase 'cc000002'   -S -d0 --rno -- '-33554439'
testcase 'cc000002'   -S -d0 --rno -- '-33554440'
testcase '4c000000'   -S -d0 --rnz --  '33554432'
testcase '4c000000'   -S -d0 --rnz --  '33554433'
testcase '4c000000'   -S -d0 --rnz --  '33554434'
testcase '4c000001'   -S -d0 --rnz --  '33554435'
testcase '4c000001'   -S -d0 --rnz --  '33554436'
testcase '4c000001'   -S -d0 --rnz --  '33554437'
testcase '4c000001'   -S -d0 --rnz --  '33554438'
testcase '4c000002'   -S -d0 --rnz --  '33554439'
testcase '4c000002'   -S -d0 --rnz --  '33554440'
testcase 'cc000000'   -S -d0 --rnz -- '-33554432'
testcase 'cc000000'   -S -d0 --rnz -- '-33554433'
testcase 'cc000000'   -S -d0 --rnz -- '-33554434'
testcase 'cc000001'   -S -d0 --rnz -- '-33554435'
testcase 'cc000001'   -S -d0 --rnz -- '-33554436'
testcase 'cc000001'   -S -d0 --rnz -- '-33554437'
testcase 'cc000001'   -S -d0 --rnz -- '-33554438'
testcase 'cc000002'   -S -d0 --rnz -- '-33554439'
testcase 'cc000002'   -S -d0 --rnz -- '-33554440'
testcase '4c000000'   -S -d0 --rni --  '33554432'
testcase '4c000000'   -S -d0 --rni --  '33554433'
testcase '4c000001'   -S -d0 --rni --  '33554434'
testcase '4c000001'   -S -d0 --rni --  '33554435'
testcase '4c000001'   -S -d0 --rni --  '33554436'
testcase '4c000001'   -S -d0 --rni --  '33554437'
testcase '4c000002'   -S -d0 --rni --  '33554438'
testcase '4c000002'   -S -d0 --rni --  '33554439'
testcase '4c000002'   -S -d0 --rni --  '33554440'
testcase 'cc000000'   -S -d0 --rni -- '-33554432'
testcase 'cc000000'   -S -d0 --rni -- '-33554433'
testcase 'cc000001'   -S -d0 --rni -- '-33554434'
testcase 'cc000001'   -S -d0 --rni -- '-33554435'
testcase 'cc000001'   -S -d0 --rni -- '-33554436'
testcase 'cc000001'   -S -d0 --rni -- '-33554437'
testcase 'cc000002'   -S -d0 --rni -- '-33554438'
testcase 'cc000002'   -S -d0 --rni -- '-33554439'
testcase 'cc000002'   -S -d0 --rni -- '-33554440'
testcase '4c000000'   -S -d0 --rnu --  '33554432'
testcase '4c000000'   -S -d0 --rnu --  '33554433'
testcase '4c000001'   -S -d0 --rnu --  '33554434'
testcase '4c000001'   -S -d0 --rnu --  '33554435'
testcase '4c000001'   -S -d0 --rnu --  '33554436'
testcase '4c000001'   -S -d0 --rnu --  '33554437'
testcase '4c000002'   -S -d0 --rnu --  '33554438'
testcase '4c000002'   -S -d0 --rnu --  '33554439'
testcase '4c000002'   -S -d0 --rnu --  '33554440'
testcase 'cc000000'   -S -d0 --rnu -- '-33554432'
testcase 'cc000000'   -S -d0 --rnu -- '-33554433'
testcase 'cc000000'   -S -d0 --rnu -- '-33554434'
testcase 'cc000001'   -S -d0 --rnu -- '-33554435'
testcase 'cc000001'   -S -d0 --rnu -- '-33554436'
testcase 'cc000001'   -S -d0 --rnu -- '-33554437'
testcase 'cc000001'   -S -d0 --rnu -- '-33554438'
testcase 'cc000002'   -S -d0 --rnu -- '-33554439'
testcase 'cc000002'   -S -d0 --rnu -- '-33554440'
testcase '4c000000'   -S -d0 --rnd --  '33554432'
testcase '4c000000'   -S -d0 --rnd --  '33554433'
testcase '4c000000'   -S -d0 --rnd --  '33554434'
testcase '4c000001'   -S -d0 --rnd --  '33554435'
testcase '4c000001'   -S -d0 --rnd --  '33554436'
testcase '4c000001'   -S -d0 --rnd --  '33554437'
testcase '4c000001'   -S -d0 --rnd --  '33554438'
testcase '4c000002'   -S -d0 --rnd --  '33554439'
testcase '4c000002'   -S -d0 --rnd --  '33554440'
testcase 'cc000000'   -S -d0 --rnd -- '-33554432'
testcase 'cc000000'   -S -d0 --rnd -- '-33554433'
testcase 'cc000001'   -S -d0 --rnd -- '-33554434'
testcase 'cc000001'   -S -d0 --rnd -- '-33554435'
testcase 'cc000001'   -S -d0 --rnd -- '-33554436'
testcase 'cc000001'   -S -d0 --rnd -- '-33554437'
testcase 'cc000002'   -S -d0 --rnd -- '-33554438'
testcase 'cc000002'   -S -d0 --rnd -- '-33554439'
testcase 'cc000002'   -S -d0 --rnd -- '-33554440'

# Tests of rounding modes in the round_rXX() function family
testcase  '8388608' -- 'round_rz ( 33554432/4)'
testcase  '8388608' -- 'round_rz ( 33554433/4)'
testcase  '8388608' -- 'round_rz ( 33554434/4)'
testcase  '8388608' -- 'round_rz ( 33554435/4)'
testcase  '8388609' -- 'round_rz ( 33554436/4)'
testcase  '8388609' -- 'round_rz ( 33554437/4)'
testcase  '8388609' -- 'round_rz ( 33554438/4)'
testcase  '8388609' -- 'round_rz ( 33554439/4)'
testcase  '8388610' -- 'round_rz ( 33554440/4)'
testcase '-8388608' -- 'round_rz (-33554432/4)'
testcase '-8388608' -- 'round_rz (-33554433/4)'
testcase '-8388608' -- 'round_rz (-33554434/4)'
testcase '-8388608' -- 'round_rz (-33554435/4)'
testcase '-8388609' -- 'round_rz (-33554436/4)'
testcase '-8388609' -- 'round_rz (-33554437/4)'
testcase '-8388609' -- 'round_rz (-33554438/4)'
testcase '-8388609' -- 'round_rz (-33554439/4)'
testcase '-8388610' -- 'round_rz (-33554440/4)'
testcase  '8388608' -- 'round_ri ( 33554432/4)'
testcase  '8388609' -- 'round_ri ( 33554433/4)'
testcase  '8388609' -- 'round_ri ( 33554434/4)'
testcase  '8388609' -- 'round_ri ( 33554435/4)'
testcase  '8388609' -- 'round_ri ( 33554436/4)'
testcase  '8388610' -- 'round_ri ( 33554437/4)'
testcase  '8388610' -- 'round_ri ( 33554438/4)'
testcase  '8388610' -- 'round_ri ( 33554439/4)'
testcase  '8388610' -- 'round_ri ( 33554440/4)'
testcase '-8388608' -- 'round_ri (-33554432/4)'
testcase '-8388609' -- 'round_ri (-33554433/4)'
testcase '-8388609' -- 'round_ri (-33554434/4)'
testcase '-8388609' -- 'round_ri (-33554435/4)'
testcase '-8388609' -- 'round_ri (-33554436/4)'
testcase '-8388610' -- 'round_ri (-33554437/4)'
testcase '-8388610' -- 'round_ri (-33554438/4)'
testcase '-8388610' -- 'round_ri (-33554439/4)'
testcase '-8388610' -- 'round_ri (-33554440/4)'
testcase  '8388608' -- 'round_ru ( 33554432/4)'
testcase  '8388609' -- 'round_ru ( 33554433/4)'
testcase  '8388609' -- 'round_ru ( 33554434/4)'
testcase  '8388609' -- 'round_ru ( 33554435/4)'
testcase  '8388609' -- 'round_ru ( 33554436/4)'
testcase  '8388610' -- 'round_ru ( 33554437/4)'
testcase  '8388610' -- 'round_ru ( 33554438/4)'
testcase  '8388610' -- 'round_ru ( 33554439/4)'
testcase  '8388610' -- 'round_ru ( 33554440/4)'
testcase '-8388608' -- 'round_ru (-33554432/4)'
testcase '-8388608' -- 'round_ru (-33554433/4)'
testcase '-8388608' -- 'round_ru (-33554434/4)'
testcase '-8388608' -- 'round_ru (-33554435/4)'
testcase '-8388609' -- 'round_ru (-33554436/4)'
testcase '-8388609' -- 'round_ru (-33554437/4)'
testcase '-8388609' -- 'round_ru (-33554438/4)'
testcase '-8388609' -- 'round_ru (-33554439/4)'
testcase '-8388610' -- 'round_ru (-33554440/4)'
testcase  '8388608' -- 'round_rd ( 33554432/4)'
testcase  '8388608' -- 'round_rd ( 33554433/4)'
testcase  '8388608' -- 'round_rd ( 33554434/4)'
testcase  '8388608' -- 'round_rd ( 33554435/4)'
testcase  '8388609' -- 'round_rd ( 33554436/4)'
testcase  '8388609' -- 'round_rd ( 33554437/4)'
testcase  '8388609' -- 'round_rd ( 33554438/4)'
testcase  '8388609' -- 'round_rd ( 33554439/4)'
testcase  '8388610' -- 'round_rd ( 33554440/4)'
testcase '-8388608' -- 'round_rd (-33554432/4)'
testcase '-8388609' -- 'round_rd (-33554433/4)'
testcase '-8388609' -- 'round_rd (-33554434/4)'
testcase '-8388609' -- 'round_rd (-33554435/4)'
testcase '-8388609' -- 'round_rd (-33554436/4)'
testcase '-8388610' -- 'round_rd (-33554437/4)'
testcase '-8388610' -- 'round_rd (-33554438/4)'
testcase '-8388610' -- 'round_rd (-33554439/4)'
testcase '-8388610' -- 'round_rd (-33554440/4)'
testcase  '8388608' -- 'round_rne( 33554432/4)'
testcase  '8388608' -- 'round_rne( 33554433/4)'
testcase  '8388608' -- 'round_rne( 33554434/4)'
testcase  '8388609' -- 'round_rne( 33554435/4)'
testcase  '8388609' -- 'round_rne( 33554436/4)'
testcase  '8388609' -- 'round_rne( 33554437/4)'
testcase  '8388610' -- 'round_rne( 33554438/4)'
testcase  '8388610' -- 'round_rne( 33554439/4)'
testcase  '8388610' -- 'round_rne( 33554440/4)'
testcase '-8388608' -- 'round_rne(-33554432/4)'
testcase '-8388608' -- 'round_rne(-33554433/4)'
testcase '-8388608' -- 'round_rne(-33554434/4)'
testcase '-8388609' -- 'round_rne(-33554435/4)'
testcase '-8388609' -- 'round_rne(-33554436/4)'
testcase '-8388609' -- 'round_rne(-33554437/4)'
testcase '-8388610' -- 'round_rne(-33554438/4)'
testcase '-8388610' -- 'round_rne(-33554439/4)'
testcase '-8388610' -- 'round_rne(-33554440/4)'
testcase  '8388608' -- 'round_rno( 33554432/4)'
testcase  '8388608' -- 'round_rno( 33554433/4)'
testcase  '8388609' -- 'round_rno( 33554434/4)'
testcase  '8388609' -- 'round_rno( 33554435/4)'
testcase  '8388609' -- 'round_rno( 33554436/4)'
testcase  '8388609' -- 'round_rno( 33554437/4)'
testcase  '8388609' -- 'round_rno( 33554438/4)'
testcase  '8388610' -- 'round_rno( 33554439/4)'
testcase  '8388610' -- 'round_rno( 33554440/4)'
testcase '-8388608' -- 'round_rno(-33554432/4)'
testcase '-8388608' -- 'round_rno(-33554433/4)'
testcase '-8388609' -- 'round_rno(-33554434/4)'
testcase '-8388609' -- 'round_rno(-33554435/4)'
testcase '-8388609' -- 'round_rno(-33554436/4)'
testcase '-8388609' -- 'round_rno(-33554437/4)'
testcase '-8388609' -- 'round_rno(-33554438/4)'
testcase '-8388610' -- 'round_rno(-33554439/4)'
testcase '-8388610' -- 'round_rno(-33554440/4)'
testcase  '8388608' -- 'round_rnz( 33554432/4)'
testcase  '8388608' -- 'round_rnz( 33554433/4)'
testcase  '8388608' -- 'round_rnz( 33554434/4)'
testcase  '8388609' -- 'round_rnz( 33554435/4)'
testcase  '8388609' -- 'round_rnz( 33554436/4)'
testcase  '8388609' -- 'round_rnz( 33554437/4)'
testcase  '8388609' -- 'round_rnz( 33554438/4)'
testcase  '8388610' -- 'round_rnz( 33554439/4)'
testcase  '8388610' -- 'round_rnz( 33554440/4)'
testcase '-8388608' -- 'round_rnz(-33554432/4)'
testcase '-8388608' -- 'round_rnz(-33554433/4)'
testcase '-8388608' -- 'round_rnz(-33554434/4)'
testcase '-8388609' -- 'round_rnz(-33554435/4)'
testcase '-8388609' -- 'round_rnz(-33554436/4)'
testcase '-8388609' -- 'round_rnz(-33554437/4)'
testcase '-8388609' -- 'round_rnz(-33554438/4)'
testcase '-8388610' -- 'round_rnz(-33554439/4)'
testcase '-8388610' -- 'round_rnz(-33554440/4)'
testcase  '8388608' -- 'round_rni( 33554432/4)'
testcase  '8388608' -- 'round_rni( 33554433/4)'
testcase  '8388609' -- 'round_rni( 33554434/4)'
testcase  '8388609' -- 'round_rni( 33554435/4)'
testcase  '8388609' -- 'round_rni( 33554436/4)'
testcase  '8388609' -- 'round_rni( 33554437/4)'
testcase  '8388610' -- 'round_rni( 33554438/4)'
testcase  '8388610' -- 'round_rni( 33554439/4)'
testcase  '8388610' -- 'round_rni( 33554440/4)'
testcase '-8388608' -- 'round_rni(-33554432/4)'
testcase '-8388608' -- 'round_rni(-33554433/4)'
testcase '-8388609' -- 'round_rni(-33554434/4)'
testcase '-8388609' -- 'round_rni(-33554435/4)'
testcase '-8388609' -- 'round_rni(-33554436/4)'
testcase '-8388609' -- 'round_rni(-33554437/4)'
testcase '-8388610' -- 'round_rni(-33554438/4)'
testcase '-8388610' -- 'round_rni(-33554439/4)'
testcase '-8388610' -- 'round_rni(-33554440/4)'
testcase  '8388608' -- 'round_rnu( 33554432/4)'
testcase  '8388608' -- 'round_rnu( 33554433/4)'
testcase  '8388609' -- 'round_rnu( 33554434/4)'
testcase  '8388609' -- 'round_rnu( 33554435/4)'
testcase  '8388609' -- 'round_rnu( 33554436/4)'
testcase  '8388609' -- 'round_rnu( 33554437/4)'
testcase  '8388610' -- 'round_rnu( 33554438/4)'
testcase  '8388610' -- 'round_rnu( 33554439/4)'
testcase  '8388610' -- 'round_rnu( 33554440/4)'
testcase '-8388608' -- 'round_rnu(-33554432/4)'
testcase '-8388608' -- 'round_rnu(-33554433/4)'
testcase '-8388608' -- 'round_rnu(-33554434/4)'
testcase '-8388609' -- 'round_rnu(-33554435/4)'
testcase '-8388609' -- 'round_rnu(-33554436/4)'
testcase '-8388609' -- 'round_rnu(-33554437/4)'
testcase '-8388609' -- 'round_rnu(-33554438/4)'
testcase '-8388610' -- 'round_rnu(-33554439/4)'
testcase '-8388610' -- 'round_rnu(-33554440/4)'
testcase  '8388608' -- 'round_rnd( 33554432/4)'
testcase  '8388608' -- 'round_rnd( 33554433/4)'
testcase  '8388608' -- 'round_rnd( 33554434/4)'
testcase  '8388609' -- 'round_rnd( 33554435/4)'
testcase  '8388609' -- 'round_rnd( 33554436/4)'
testcase  '8388609' -- 'round_rnd( 33554437/4)'
testcase  '8388609' -- 'round_rnd( 33554438/4)'
testcase  '8388610' -- 'round_rnd( 33554439/4)'
testcase  '8388610' -- 'round_rnd( 33554440/4)'
testcase '-8388608' -- 'round_rnd(-33554432/4)'
testcase '-8388608' -- 'round_rnd(-33554433/4)'
testcase '-8388609' -- 'round_rnd(-33554434/4)'
testcase '-8388609' -- 'round_rnd(-33554435/4)'
testcase '-8388609' -- 'round_rnd(-33554436/4)'
testcase '-8388609' -- 'round_rnd(-33554437/4)'
testcase '-8388610' -- 'round_rnd(-33554438/4)'
testcase '-8388610' -- 'round_rnd(-33554439/4)'
testcase '-8388610' -- 'round_rnd(-33554440/4)'
testcase  '8388608' --debug-fns -- 'round_rz ( 33554432/4 + abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rz ( 33554436/4 + abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_rz ( 33554440/4 + abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_rz (-33554432/4 - abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rz (-33554436/4 - abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_rz (-33554440/4 - abs(Stealth1-1))'
testcase  '8388608' --debug-fns -- 'round_ri ( 33554432/4 - abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_ri ( 33554436/4 - abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_ri ( 33554440/4 - abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_ri (-33554432/4 + abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_ri (-33554436/4 + abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_ri (-33554440/4 + abs(Stealth1-1))'
testcase  '8388608' --debug-fns -- 'round_ru ( 33554432/4 - abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_ru ( 33554436/4 - abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_ru ( 33554440/4 - abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_ru (-33554432/4 - abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_ru (-33554436/4 - abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_ru (-33554440/4 - abs(Stealth1-1))'
testcase  '8388608' --debug-fns -- 'round_rd ( 33554432/4 + abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rd ( 33554436/4 + abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_rd ( 33554440/4 + abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_rd (-33554432/4 + abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rd (-33554436/4 + abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_rd (-33554440/4 + abs(Stealth1-1))'
testcase  '8388608' --debug-fns -- 'round_rne( 33554434/4 - abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_rne( 33554438/4 + abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_rne(-33554434/4 + abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_rne(-33554438/4 - abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rno( 33554434/4 + abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rno( 33554438/4 - abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rno(-33554434/4 - abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rno(-33554438/4 + abs(Stealth1-1))'
testcase  '8388608' --debug-fns -- 'round_rnz( 33554434/4 - abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rnz( 33554438/4 - abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_rnz(-33554434/4 + abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rnz(-33554438/4 + abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rni( 33554434/4 + abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_rni( 33554438/4 + abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rni(-33554434/4 - abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_rni(-33554438/4 - abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rnu( 33554434/4 + abs(Stealth1-1))'
testcase  '8388610' --debug-fns -- 'round_rnu( 33554438/4 + abs(Stealth1-1))'
testcase '-8388608' --debug-fns -- 'round_rnu(-33554434/4 + abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rnu(-33554438/4 + abs(Stealth1-1))'
testcase  '8388608' --debug-fns -- 'round_rnd( 33554434/4 - abs(Stealth1-1))'
testcase  '8388609' --debug-fns -- 'round_rnd( 33554438/4 - abs(Stealth1-1))'
testcase '-8388609' --debug-fns -- 'round_rnd(-33554434/4 - abs(Stealth1-1))'
testcase '-8388610' --debug-fns -- 'round_rnd(-33554438/4 - abs(Stealth1-1))'

# Miscellaneous tests
testcase '3fc00000' -S -d10 -- 'cbrt(ieee:40580000)'
testcase '262537412640768743.9999999999992500725971981856888793538563'    -d40 -- 'exp(pi*sqrt(163))'
testcase '19.9990999791894757672664429846690444960689368432251061724701018172165259' -d70 -- 'exp(pi)-pi' # xkcd #217
testcase '7feffffffffffe50.ea4b00a2671de9ec' -D -d64 -- 'gamma(ieee:406573fae561f647)'
testcase '119.7954512922427202889689745352405411702723'    -d40 -- 'gamma(5.999)'
testcase '120'    -d40 -- 'gamma(6)'
testcase '120.2049197671270571426949011864693528903251'    -d40 -- 'gamma(6.001)'
testcase '7fa4ab78644182e7.36582dd9ca' -D -d40 -- 'gamma(ieee:40655fffffffffff)'
testcase '7fa4ab7864418638.f2203ba4f8' -D -d40 -- 'gamma(ieee:4065600000000000)'
testcase '7fa4ab786441898a.ade84970af' -D -d40 -- 'gamma(ieee:4065600000000001)'
testcase '0.8427007929497148693412206350826092592960'    -d40 -- 'erf(1)'
testcase '-0.8427007929497148693412206350826092592960'    -d40 -- 'erf(-1)'
testcase '0.1572992070502851306587793649173907407039'    -d40 -- 'erfc(1)'
testcase '1.8427007929497148693412206350826092592960'    -d40 -- 'erfc(-1)'
testcase '0.8413447460685429485852325456320379224779'    -d40 -- 'Phi(1)'
testcase '0.1586552539314570514147674543679620775220'    -d40 -- 'Phi(-1)'
testcase 'c0600000' -S -d32 -- '-3.5'
testcase 'c0490fda.a22168c2' -S -d32 -- '-pi'
testcase '0' -d70 -- 'abs(0)'
testcase '0.4' -d70 -- 'abs(0.4)'
testcase '0.4' -d70 -- 'abs(-0.4)'
testcase '3.1415926535897932384626433832795028841971693993751058209749445923078164' -d70 -- 'abs(pi)'
testcase '3.1415926535897932384626433832795028841971693993751058209749445923078164' -d70 -- 'abs(-pi)'
testcase '0' -- 'sign(0)'
testcase '1' -- 'sign(0.4)'
testcase '-1' -- 'sign(-0.4)'
testcase '1' -- 'sign(pi)'
testcase '-1' -- 'sign(-pi)'
testcase '11.383358' --printf=%f 'W(1000000)'
testcase '-5.997807' --printf=%f 'invnorm(0.000000001)'
testcase '4.47550205634926123052849152389606512569258015067144' -d50 -- 'sqrt(-log(0.000000002))'
testcase '21430172143725346418968500981200036211228096234110672148875007767407021022498722449863967576313917162551893458351062936503742905713846280871969155149397149607869135549648461970842149210124742283755908364306092949967163882534797535118331087892154125829142392955373084335320859663305248773674411336138752' 2^1001
testcase '25372623506747431611343734828921129692212922067271259852235159388169369957552784264144600916821700667857864' 293847358972094572309547230945702354**3
testcase '0.1104916011444797210345658364367297409996306334639247224269963542020706' -d70 'W(0.1234)'
testcase '-1' '0*exp(0)-1'
testcase '8.7744375108173427218060841592172476313972' -d40 '15*log2(1.5)'
testcase '0' 'asin(0)'
testcase '1.5707963267948966192313216916397514420985' -d40 'asin(1)'
testcase '-1.5707963267948966192313216916397514420985' -d40 'asin(-1)'
testcase '0' 'acos(1)'
testcase '3.8082593202564599051293100499461695508638' -d40 'pi + (2/3)'
testcase '3.8082593202564599051293100499461695508638' -d40 '(2/3) + pi'
testcase '2.4749259869231265717959767166128362175305' -d40 'pi - (2/3)'
testcase '-2.4749259869231265717959767166128362175305' -d40 '(2/3) - pi'
testcase '2.0943951023931954923084289221863352561314' -d40 'pi * (2/3)'
testcase '2.0943951023931954923084289221863352561314' -d40 '(2/3) * pi'
testcase '4.7123889803846898576939650749192543262957' -d40 'pi / (2/3)'
testcase '0.2122065907891937810251783511633524827126' -d40 '(2/3) / pi'
testcase '1' 'ceil(1+sqrt(0))'
testcase '1' 'ceil(1-sqrt(0))'
testcase '1' 'ceil(1)'
testcase '-0.41551544396166582316' -d20 'log(0.66)'
testcase '0;1,75,1,2,4,2,23,2,6,1,4,1,1,2,26,1,13,1,2,4' -d20 -cl -- 'pi^2/10'
testcase '1' '1^0.5'
testcase '1' '1^1.5'
testcase '-0.1' --printf=%.17g -- '-0.1'
testcase '0' 'log2(1)'
testcase '1' 'log2(2)'
testcase '2' 'log2(4)'
testcase '3' 'log2(8)'
testcase '456' 'log2(2^456)'
testcase '-1' 'log2(1/2)'
testcase '-2' 'log2(1/4)'
testcase '-3' 'log2(1/8)'
testcase '-456' 'log2(2^-456)'
testcase '0' 'log10(1)'
testcase '1' 'log10(10)'
testcase '2' 'log10(100)'
testcase '3' 'log10(1000)'
testcase '456' 'log10(10^456)'
testcase '-1' 'log10(1/10)'
testcase '-2' 'log10(1/100)'
testcase '-3' 'log10(1/1000)'
testcase '-456' 'log10(10^-456)'
# some tests of dyadic log
testcase '2' -R 'log(4,2)'
testcase '1/2' -R 'log(2,4)'
testcase '3/5' -R 'log(1000,100000)'
testcase '3/5' -R 'log(1/1000,1/100000)'
testcase '-3/5' -R 'log(1000,1/100000)'
testcase '-3/5' -R 'log(1/1000,100000)'
testcase '5/3' -R 'log(100000,1000)'
testcase '5/3' -R 'log(1/100000,1/1000)'
testcase '-5/3' -R 'log(1/100000,1000)'
testcase '-5/3' -R 'log(100000,1/1000)'
testcase '2/3' -R 'log(25/9,125/27)'
testcase '-2/3' -R 'log(25/9,27/125)'
testcase '0.72105705434887015680' -d20 'log(6,12)' # and test the fallback!
# more miscellaneous
testcase '2' '8^{1/3}'
testcase '1' '0!'
testcase '1' '1!'
testcase '2' '2!'
testcase '6' '3!'
testcase '729' '3^3!'
testcase '729' '3^(3!)'
testcase '10888869450418352160768000000' '(3^3)!'

# Zero inputs to the erf family
testcase '0' 'erf(0)'
testcase '1' 'erfc(0)'
testcase '0.5' 'Phi(0)'

# Basic tests of functions implemented via NewtonInverter.
testcase '1.8213863677184496730402103186209952434812288836009480886250922720318072' -d70 -- 'inverf(0.99)'
testcase '-1.8213863677184496730402103186209952434812288836009480886250922720318072' -d70 -- 'inverf(-0.99)'
testcase '0.4769362762044698733814183536431305598089697490594706447038826959193834' -d70 -- 'inverf(0.5)'
testcase '-0.4769362762044698733814183536431305598089697490594706447038826959193834' -d70 -- 'inverf(-0.5)'
testcase '0.0088625012809505979078013935535131353941962584770623078524783163210542' -d70 -- 'inverfc(0.99)'
testcase '-0.0088625012809505979078013935535131353941962584770623078524783163210542' -d70 -- 'inverfc(1.01)'
testcase '2.3263478740408411008856061633469117233518171415320130690656402478908766' -d70 -- 'invPhi(0.99)'
testcase '-2.3263478740408411008856061633469117233518171415320130690656402478908766' -d70 -- 'invPhi(0.01)'
testcase '3.9297432688046173057201477930585757277349972940194383413688364353542583' -d70 -- 'W(200)'
testcase '0.5671432904097838729999686622103555497538157871865125081351310792230457' -d70 -- 'W(1)'
testcase '0' -d70 -- 'W(0)'
testcase '-0.9999201984840833972182231127073406469776071837174794102608915537024632' -d70 -- 'W(-0.36787944)'
testcase '-1.0000798057616637770825447928580901142840764432265288536708449220027306' -d70 -- 'Wn(-0.36787944)'
testcase '-3.5771520639572972184093919635119948804017962577930759236835277557916872' -d70 -- 'Wn(-0.1)'
testcase '-19.0660024220655498959154531443190069189932864410604280639322270734998948' -d70 -- 'Wn(-0.0000001)'

# Tests of sign of gamma/lgamma
testcase  '-0.038984275923083330038784240972' -d30 'lgamma(1.9)'
testcase   '0.066376239734742971188716739867' -d30 'lgamma(0.9)'
testcase   '2.368961332728788655206708194551' -d30 'lgamma(-0.1)'
testcase   '2.273651152924463795162756071270' -d30 'lgamma(-1.1)'
testcase   '1.531713808195086482680149545589' -d30 'lgamma(-2.1)'
testcase   '0.961765831907387419407574802125' -d30 'gamma(1.9)'
testcase   '1.068628702119319354897305335694' -d30 'gamma(0.9)'
testcase '-10.686287021193193548973053356944' -d30 'gamma(-0.1)'
testcase   '9.714806382902903226339139415404' -d30 'gamma(-1.1)'
testcase  '-4.626098277572811060161494959716' -d30 'gamma(-2.1)'

# Tests of algebraic()
testcase '0.00887298334620741688517926539978239961083292170529' -d50 'algebraic(.005,1,1,-1000,100000)'
testcase '0.00112701665379258311482073460021760038916707829470' -d50 'algebraic(0,.005,1,-1000,100000)'
testcase '-2.12706205858986632920549972846925001013654801639064' -d50 'algebraic(-10,0,1,-20,0,0,0,1)'
testcase '0.05000001562502441411590589761771708837482484349643' -d50 'algebraic(0,1,1,-20,0,0,0,1)'
testcase '2.10205331612766865422237271458814061473591808060490' -d50 'algebraic(1,10,1,-20,0,0,0,1)'
# An example of spotting a rational root.
testcase '1;2,3' -d100 -cl 'algebraic(1,2,-10,307,-210,0,0,-10,7)'
# Coping with repeated roots: a polynomial and its square should work
# the same.
testcase '1.61803398874989484820458683436563811772030917980576' -d50 'algebraic(0,2,-1,-1,+1)'
testcase '-0.61803398874989484820458683436563811772030917980576' -d50 'algebraic(-2,0,-1,-1,+1)'
testcase '1.61803398874989484820458683436563811772030917980576' -d50 'algebraic(0,2,+1,+2,-1,-2,+1)'
testcase '-0.61803398874989484820458683436563811772030917980576' -d50 'algebraic(-2,0,+1,+2,-1,-2,+1)'
# Make sure negating the whole polynomial doesn't confuse matters.
testcase '1.61803398874989484820458683436563811772030917980576' -d50 'algebraic(0,2,-1,-1,+1)'
testcase '1.61803398874989484820458683436563811772030917980576' -d50 'algebraic(0,2,+1,+1,-1)'
# And show that we can use rational as well as integer coefficients.
testcase '1.61803398874989484820458683436563811772030917980576' -d50 'algebraic(0,2,-.1,-.1,+.1)'
# A well-known algebraic number: Conway's constant. (Continued
# fraction terms from OEIS A014967; polynomial coefficients from OEIS
# A137275.)
testcase '1;3,3,2,2,54,5,2,1,16,1,30,1,1,1,2,2,1,14,1,6,24,107,5,1,1,26,2,41,10,1,1,5,17,5,1,8,3,94,38,1,18,1,1,2,1,64,18,1,6,1,2,2,1,23,1,4,4,1,1,3,4,1,10,1,28,4,12,1,1238,13,1,1,58,1,2,4,1,3,7,1,3,1,4,1,1,1,1,1,1,100' -d90 -cl 'algebraic(0,2,-6,3,-6,12,-4,7,-7,1,0,5,-2,-4,-12,2,7,12,-7,-10,-4,3,9,-7,0,-8,14,-3,9,2,-3,-10,-2,-6,1,10,-3,1,7,-7,7,-12,-5,8,6,10,-8,-8,-7,-3,9,1,6,6,-2,-3,-10,-2,3,5,2,-1,-1,-1,-1,-1,1,2,2,-1,-2,-1,0,1)'
# Check that the special case of pow() implemented via algebraic is
# working.
testcase '0.17677669529663688110' -d20 -- '(+2)^(-10/4)'
testcase '0.21022410381342863575' -d20 -- '(+2)^(-9/4)'
testcase '0.25' -d20 -- '(+2)^(-8/4)'
testcase '0.29730177875068026667' -d20 -- '(+2)^(-7/4)'
testcase '0.35355339059327376220' -d20 -- '(+2)^(-6/4)'
testcase '0.42044820762685727151' -d20 -- '(+2)^(-5/4)'
testcase '0.5' -d20 -- '(+2)^(-4/4)'
testcase '0.59460355750136053335' -d20 -- '(+2)^(-3/4)'
testcase '0.70710678118654752440' -d20 -- '(+2)^(-2/4)'
testcase '0.84089641525371454303' -d20 -- '(+2)^(-1/4)'
testcase '1' -d20 -- '(+2)^(0/4)'
testcase '1.18920711500272106671' -d20 -- '(+2)^(1/4)'
testcase '1.41421356237309504880' -d20 -- '(+2)^(2/4)'
testcase '1.68179283050742908606' -d20 -- '(+2)^(3/4)'
testcase '2' -d20 -- '(+2)^(4/4)'
testcase '2.37841423000544213343' -d20 -- '(+2)^(5/4)'
testcase '2.82842712474619009760' -d20 -- '(+2)^(6/4)'
testcase '3.36358566101485817212' -d20 -- '(+2)^(7/4)'
testcase '4' -d20 -- '(+2)^(8/4)'
testcase '4.75682846001088426686' -d20 -- '(+2)^(9/4)'
testcase '5.65685424949238019520' -d20 -- '(+2)^(10/4)'
testcase '0.25' -d20 -- '(+2)^(-10/5)'
testcase '0.28717458874925875169' -d20 -- '(+2)^(-9/5)'
testcase '0.32987697769322356484' -d20 -- '(+2)^(-8/5)'
testcase '0.37892914162759952058' -d20 -- '(+2)^(-7/5)'
testcase '0.43527528164806206956' -d20 -- '(+2)^(-6/5)'
testcase '0.5' -d20 -- '(+2)^(-5/5)'
testcase '0.57434917749851750339' -d20 -- '(+2)^(-4/5)'
testcase '0.65975395538644712968' -d20 -- '(+2)^(-3/5)'
testcase '0.75785828325519904117' -d20 -- '(+2)^(-2/5)'
testcase '0.87055056329612413913' -d20 -- '(+2)^(-1/5)'
testcase '1' -d20 -- '(+2)^(0/5)'
testcase '1.14869835499703500679' -d20 -- '(+2)^(1/5)'
testcase '1.31950791077289425937' -d20 -- '(+2)^(2/5)'
testcase '1.51571656651039808234' -d20 -- '(+2)^(3/5)'
testcase '1.74110112659224827827' -d20 -- '(+2)^(4/5)'
testcase '2' -d20 -- '(+2)^(5/5)'
testcase '2.29739670999407001359' -d20 -- '(+2)^(6/5)'
testcase '2.63901582154578851874' -d20 -- '(+2)^(7/5)'
testcase '3.03143313302079616469' -d20 -- '(+2)^(8/5)'
testcase '3.48220225318449655654' -d20 -- '(+2)^(9/5)'
testcase '4' -d20 -- '(+2)^(10/5)'
testcase '0.25' -d20 -- '(-2)^(-8/4)'
testcase '-0.5' -d20 -- '(-2)^(-4/4)'
testcase '1' -d20 -- '(-2)^(0/4)'
testcase '-2' -d20 -- '(-2)^(4/4)'
testcase '4' -d20 -- '(-2)^(8/4)'
testcase '0.25' -d20 -- '(-2)^(-10/5)'
testcase '-0.28717458874925875169' -d20 -- '(-2)^(-9/5)'
testcase '0.32987697769322356484' -d20 -- '(-2)^(-8/5)'
testcase '-0.37892914162759952058' -d20 -- '(-2)^(-7/5)'
testcase '0.43527528164806206956' -d20 -- '(-2)^(-6/5)'
testcase '-0.5' -d20 -- '(-2)^(-5/5)'
testcase '0.57434917749851750339' -d20 -- '(-2)^(-4/5)'
testcase '-0.65975395538644712968' -d20 -- '(-2)^(-3/5)'
testcase '0.75785828325519904117' -d20 -- '(-2)^(-2/5)'
testcase '-0.87055056329612413913' -d20 -- '(-2)^(-1/5)'
testcase '1' -d20 -- '(-2)^(0/5)'
testcase '-1.14869835499703500679' -d20 -- '(-2)^(1/5)'
testcase '1.31950791077289425937' -d20 -- '(-2)^(2/5)'
testcase '-1.51571656651039808234' -d20 -- '(-2)^(3/5)'
testcase '1.74110112659224827827' -d20 -- '(-2)^(4/5)'
testcase '-2' -d20 -- '(-2)^(5/5)'
testcase '2.29739670999407001359' -d20 -- '(-2)^(6/5)'
testcase '-2.63901582154578851874' -d20 -- '(-2)^(7/5)'
testcase '3.03143313302079616469' -d20 -- '(-2)^(8/5)'
testcase '-3.48220225318449655654' -d20 -- '(-2)^(9/5)'
testcase '4' -d20 -- '(-2)^(10/5)'

# Check other special cases of pow.
testcase '1' -d70 'pi^0'
testcase '0' -d70 '0^pi'
testcase '3.1415926535897932384626433832795028841971693993751058209749445923078164' -d70 'pi^1'
testcase '0.3183098861837906715377675267450287240689192914809128974953346881177935' -d70 'pi^-1'

# Tests of degree-scaled trig functions
testcase '0' -d6 'sind(-360)'
testcase '0.5' -d6 'sind(-330)'
testcase '0.866025' -d6 'sind(-300)'
testcase '1' -d6 'sind(-270)'
testcase '0.866025' -d6 'sind(-240)'
testcase '0.5' -d6 'sind(-210)'
testcase '0' -d6 'sind(-180)'
testcase '-0.5' -d6 'sind(-150)'
testcase '-0.866025' -d6 'sind(-120)'
testcase '-1' -d6 'sind(-90)'
testcase '-0.866025' -d6 'sind(-60)'
testcase '-0.5' -d6 'sind(-30)'
testcase '0' -d6 'sind(0)'
testcase '0.5' -d6 'sind(30)'
testcase '0.866025' -d6 'sind(60)'
testcase '1' -d6 'sind(90)'
testcase '0.866025' -d6 'sind(120)'
testcase '0.5' -d6 'sind(150)'
testcase '0' -d6 'sind(180)'
testcase '-0.5' -d6 'sind(210)'
testcase '-0.866025' -d6 'sind(240)'
testcase '-1' -d6 'sind(270)'
testcase '-0.866025' -d6 'sind(300)'
testcase '-0.5' -d6 'sind(330)'
testcase '0' -d6 'sind(360)'
testcase '1' -d6 'cosd(-360)'
testcase '0.866025' -d6 'cosd(-330)'
testcase '0.5' -d6 'cosd(-300)'
testcase '0' -d6 'cosd(-270)'
testcase '-0.5' -d6 'cosd(-240)'
testcase '-0.866025' -d6 'cosd(-210)'
testcase '-1' -d6 'cosd(-180)'
testcase '-0.866025' -d6 'cosd(-150)'
testcase '-0.5' -d6 'cosd(-120)'
testcase '0' -d6 'cosd(-90)'
testcase '0.5' -d6 'cosd(-60)'
testcase '0.866025' -d6 'cosd(-30)'
testcase '1' -d6 'cosd(0)'
testcase '0.866025' -d6 'cosd(30)'
testcase '0.5' -d6 'cosd(60)'
testcase '0' -d6 'cosd(90)'
testcase '-0.5' -d6 'cosd(120)'
testcase '-0.866025' -d6 'cosd(150)'
testcase '-1' -d6 'cosd(180)'
testcase '-0.866025' -d6 'cosd(210)'
testcase '-0.5' -d6 'cosd(240)'
testcase '0' -d6 'cosd(270)'
testcase '0.5' -d6 'cosd(300)'
testcase '0.866025' -d6 'cosd(330)'
testcase '1' -d6 'cosd(360)'
testcase '0' -d6 'tand(-180)'
testcase '0.414213' -d6 'tand(-315/2)'
testcase '1' -d6 'tand(-135)'
testcase '2.414213' -d6 'tand(-225/2)'
testcase '-2.414213' -d6 'tand(-135/2)'
testcase '-1' -d6 'tand(-45)'
testcase '-0.414213' -d6 'tand(-45/2)'
testcase '0' -d6 'tand(0)'
testcase '0.414213' -d6 'tand(45/2)'
testcase '1' -d6 'tand(45)'
testcase '2.414213' -d6 'tand(135/2)'
testcase '-2.414213' -d6 'tand(225/2)'
testcase '-1' -d6 'tand(135)'
testcase '-0.414213' -d6 'tand(315/2)'
testcase '0' -d6 'tand(180)'
testcase '-90' -d6 'asind(-1)'
testcase '-48.590377' -d6 'asind(-0.75)'
testcase '-30' -d6 'asind(-0.5)'
testcase '-14.477512' -d6 'asind(-0.25)'
testcase '0' -d6 'asind(0)'
testcase '14.477512' -d6 'asind(0.25)'
testcase '30' -d6 'asind(0.5)'
testcase '48.590377' -d6 'asind(0.75)'
testcase '90' -d6 'asind(1)'
testcase '180' -d6 'acosd(-1)'
testcase '138.590377' -d6 'acosd(-0.75)'
testcase '120' -d6 'acosd(-0.5)'
testcase '104.477512' -d6 'acosd(-0.25)'
testcase '90' -d6 'acosd(0)'
testcase '75.522487' -d6 'acosd(0.25)'
testcase '60' -d6 'acosd(0.5)'
testcase '41.409622' -d6 'acosd(0.75)'
testcase '0' -d6 'acosd(1)'
testcase '-45' -d6 'atand(-1)'
testcase '-26.565051' -d6 'atand(-0.5)'
testcase '0' -d6 'atand(0)'
testcase '26.565051' -d6 'atand(0.5)'
testcase '45' -d6 'atand(1)'
testcase '36.869897' -d6 'atan2d(3,4)'
testcase '-21.801409' -d6 'atan2d(-2,5)'
testcase '98.130102' -d6 'atan2d(7,-1)'
testcase '-146.309932' -d6 'atan2d(-6,-9)'
testcase '0' -d6 'atan2d(0,1)'
testcase '180' -d6 'atan2d(0,-1)'
testcase '90' -d6 'atan2d(1,0)'
testcase '-90' -d6 'atan2d(-1,0)'
testcase '45' -d6 'atan2d(1,1)'
testcase '-45' -d6 'atan2d(-1,1)'
testcase '135' -d6 'atan2d(1,-1)'
testcase '-135' -d6 'atan2d(-1,-1)'

# Tests of --printf
testcase '3.354680e-05'                                        --printf=%e          -- 'pi^-9'
testcase '1.053904e-04'                                        --printf=%e          -- 'pi^-8'
testcase '9.242692e+05'                                        --printf=%e          -- 'pi^12'
testcase '2.903677e+06'                                        --printf=%e          -- 'pi^13'
testcase '0.000034'                                            --printf=%f          -- 'pi^-9'
testcase '0.000105'                                            --printf=%f          -- 'pi^-8'
testcase '924269.181523'                                       --printf=%f          -- 'pi^12'
testcase '2903677.270613'                                      --printf=%f          -- 'pi^13'
testcase '3.35468e-05'                                         --printf=%g          -- 'pi^-9'
testcase '0.00010539'                                          --printf=%g          -- 'pi^-8'
testcase '924269'                                              --printf=%g          -- 'pi^12'
testcase '2.90368e+06'                                         --printf=%g          -- 'pi^13'
testcase '0x1.921fb54443p+1'                                   --printf=%.10a       -- 'pi'
testcase '0x1.921fb54443p+0'                                   --printf=%.10a       -- 'pi/2'
testcase '0x1.921fb54443p-1'                                   --printf=%.10a       -- 'pi/4'
testcase '0x1.921fb54443p-2'                                   --printf=%.10a       -- 'pi/8'
testcase '0x1.921fb54443p-3'                                   --printf=%.10a       -- 'pi/16'
testcase '0x1.921fb54443p-15'                                  --printf=%.10a       -- 'pi/65536'
testcase '0x1.921fb54443p+17'                                  --printf=%.10a       -- 'pi*65536'
testcase '0x1.1969364d16p-15'                                  --printf=%.10a       -- 'pi^-9'
testcase '0x1.ba0a12450cp-14'                                  --printf=%.10a       -- 'pi^-8'
testcase '0x1.c34da5cf0ap+19'                                  --printf=%.10a       -- 'pi^12'
testcase '0x1.6273ea2a37p+21'                                  --printf=%.10a       -- 'pi^13'
testcase '-0x1.921fb54443p-15'                                 --printf=%.10a       -- '-pi/65536'
testcase '-0x1.c34da5cf0ap+19'                                 --printf=%.10a       -- '-pi^12'
testcase '                               -0x1.921fb54443p-15'  --printf='% 50.10a'  -- '-pi/65536'
testcase '-0x1.c34da5cf0ap+19                               '  --printf='%+-50.10a' -- '-pi^12'
testcase '-0x00000000000000000000000000000001.c34da5cf0ap+19'  --printf='%050.10a'  -- '-pi^12'
testcase '-0x00000000000000000000000000000001.c34da5cf0ap+19'  --printf='% 050.10a' -- '-pi^12'
testcase '-0x1.c34da5cf0ap+19                               '  --printf='%-050.10a' -- '-pi^12'
testcase '3e-05'                                               --printf=%.0e        -- 'pi^-9'
testcase '1e-04'                                               --printf=%.0e        -- 'pi^-8'
testcase '9e+05'                                               --printf=%.0e        -- 'pi^12'
testcase '3e+06'                                               --printf=%.0e        -- 'pi^13'
testcase '0'                                                   --printf=%.0f        -- 'pi^-9'
testcase '0'                                                   --printf=%.0f        -- 'pi^-8'
testcase '924269'                                              --printf=%.0f        -- 'pi^12'
testcase '2903677'                                             --printf=%.0f        -- 'pi^13'
testcase '3e-05'                                               --printf=%.0g        -- 'pi^-9'
testcase '0.0001'                                              --printf=%.0g        -- 'pi^-8'
testcase '9e+05'                                               --printf=%.0g        -- 'pi^12'
testcase '3e+06'                                               --printf=%.0g        -- 'pi^13'
testcase '3.4e-05'                                             --printf=%.1e        -- 'pi^-9'
testcase '1.1e-04'                                             --printf=%.1e        -- 'pi^-8'
testcase '9.2e+05'                                             --printf=%.1e        -- 'pi^12'
testcase '2.9e+06'                                             --printf=%.1e        -- 'pi^13'
testcase '0.0'                                                 --printf=%.1f        -- 'pi^-9'
testcase '0.0'                                                 --printf=%.1f        -- 'pi^-8'
testcase '924269.2'                                            --printf=%.1f        -- 'pi^12'
testcase '2903677.3'                                           --printf=%.1f        -- 'pi^13'
testcase '3e-05'                                               --printf=%.1g        -- 'pi^-9'
testcase '0.0001'                                              --printf=%.1g        -- 'pi^-8'
testcase '9e+05'                                               --printf=%.1g        -- 'pi^12'
testcase '3e+06'                                               --printf=%.1g        -- 'pi^13'
testcase '3.e-05'                                              --printf=%#.0e       -- 'pi^-9'
testcase '1.e-04'                                              --printf=%#.0e       -- 'pi^-8'
testcase '9.e+05'                                              --printf=%#.0e       -- 'pi^12'
testcase '3.e+06'                                              --printf=%#.0e       -- 'pi^13'
testcase '0.'                                                  --printf=%#.0f       -- 'pi^-9'
testcase '0.'                                                  --printf=%#.0f       -- 'pi^-8'
testcase '924269.'                                             --printf=%#.0f       -- 'pi^12'
testcase '2903677.'                                            --printf=%#.0f       -- 'pi^13'
testcase '3.e-05'                                              --printf=%#.0g       -- 'pi^-9'
testcase '0.0001'                                              --printf=%#.0g       -- 'pi^-8'
testcase '9.e+05'                                              --printf=%#.0g       -- 'pi^12'
testcase '3.e+06'                                              --printf=%#.0g       -- 'pi^13'
testcase '3.4e-05'                                             --printf=%#.1e       -- 'pi^-9'
testcase '1.1e-04'                                             --printf=%#.1e       -- 'pi^-8'
testcase '9.2e+05'                                             --printf=%#.1e       -- 'pi^12'
testcase '2.9e+06'                                             --printf=%#.1e       -- 'pi^13'
testcase '0.0'                                                 --printf=%#.1f       -- 'pi^-9'
testcase '0.0'                                                 --printf=%#.1f       -- 'pi^-8'
testcase '924269.2'                                            --printf=%#.1f       -- 'pi^12'
testcase '2903677.3'                                           --printf=%#.1f       -- 'pi^13'
testcase '3.e-05'                                              --printf=%#.1g       -- 'pi^-9'
testcase '0.0001'                                              --printf=%#.1g       -- 'pi^-8'
testcase '9.e+05'                                              --printf=%#.1g       -- 'pi^12'
testcase '3.e+06'                                              --printf=%#.1g       -- 'pi^13'
testcase '0x1p+0'                                              --printf=%a          ieee:3f800000
testcase '0x1.fffffep+127'                                     --printf=%a          ieee:7f7fffff
testcase '0x1p-149'                                            --printf=%a          ieee:00000001
testcase '-0x1p+0'                                             --printf=%a          ieee:bff0000000000000
testcase '-0x1.fffffffffffffp+1023'                            --printf=%a          ieee:ffefffffffffffff
testcase '-0x1p-1074'                                          --printf=%a          ieee:8000000000000001
testcase '0X1P+0'                                              --printf=%A          ieee:3f800000
testcase '3.354680E-05'                                        --printf=%E          -- 'pi^-9'
testcase '1.053904E-04'                                        --printf=%E          -- 'pi^-8'
testcase '9.242692E+05'                                        --printf=%E          -- 'pi^12'
testcase '2.903677E+06'                                        --printf=%E          -- 'pi^13'
testcase '0.000034'                                            --printf=%F          -- 'pi^-9'
testcase '0.000105'                                            --printf=%F          -- 'pi^-8'
testcase '924269.181523'                                       --printf=%F          -- 'pi^12'
testcase '2903677.270613'                                      --printf=%F          -- 'pi^13'
testcase '3.35468E-05'                                         --printf=%G          -- 'pi^-9'
testcase '0.00010539'                                          --printf=%G          -- 'pi^-8'
testcase '924269'                                              --printf=%G          -- 'pi^12'
testcase '2.90368E+06'                                         --printf=%G          -- 'pi^13'
testcase '0X1.921FB54443P+1'                                   --printf=%.10A       -- 'pi'
testcase '1.250000e+00'                                        --printf=%e          -- 1.25
testcase '1.250000'                                            --printf=%f          -- 1.25
testcase '1.25'                                                --printf=%g          -- 1.25
testcase '1.25e+07'                                            --printf=%g          -- 12500000
testcase '0x1.4p+0'                                            --printf=%a          -- 1.25
testcase '1.250000e+00'                                        --printf=%#e         -- 1.25
testcase '1.250000'                                            --printf=%#f         -- 1.25
testcase '1.25000'                                             --printf=%#g         -- 1.25
testcase '1.25000e+07'                                         --printf=%#g         -- 12500000
testcase '0x1.4p+0'                                            --printf=%#a         -- 1.25
testcase '1.2500000000e+00'                                    --printf=%.10e       -- 1.25
testcase '1.2500000000'                                        --printf=%.10f       -- 1.25
testcase '1.25'                                                --printf=%.10g       -- 1.25
testcase '12500000'                                            --printf=%.10g       -- 12500000
testcase '0x1.4000000000p+0'                                   --printf=%.10a       -- 1.25
testcase '1.2500000000e+00'                                    --printf=%#.10e      -- 1.25
testcase '1.2500000000'                                        --printf=%#.10f      -- 1.25
testcase '1.250000000'                                         --printf=%#.10g      -- 1.25
testcase '12500000.00'                                         --printf=%#.10g      -- 12500000
testcase '0x1.4000000000p+0'                                   --printf=%#.10a      -- 1.25

# Regression tests of former exactness hazards, now fixed.
testcase '1.7320508075688772935274463415058723669428052538103806280558069794519330' -d70 -- '(sqrt(2) - sqrt(2)) + sqrt(3)'
testcase '1.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 -- 'sin(pi) + sqrt(2)'
testcase '2.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 -- 'sin(pi/2) + sqrt(2)'
testcase '0.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 -- 'sin(3*pi/2) + sqrt(2)'
testcase '1.7320508075688772935274463415058723669428052538103806280558069794519330' -d70 -- 'abs(sqrt(2) - sqrt(2)) + sqrt(3)'
testcase '1.9142135623730950488016887242096980785696718753769480731766797379907324' -d70 -- '(sin(pi/6) % 10) + sqrt(2)'
testcase '0.7071067811865475244008443621048490392848359376884740365883398689953662' -d70 -- 'sqrt(sin(pi/6))'
testcase '1.7320508075688772935274463415058723669428052538103806280558069794519330' -d70 -- 'sqrt(sqrt(2) - sqrt(2)) + sqrt(3)'
testcase '2.8030395704817853465773186861049226872619959522386784412112159095083408' -d70 -- 'cbrt(cbrt(2) - cbrt(-2)) + cbrt(3)'
testcase '1.414213' -d6 -- 'erfinv(sin(pi)) + sqrt(2)'
testcase '1.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 -- 'atan(sin(pi)) + sqrt(2)'
testcase '1.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 -- 'atan2(sin(pi), 1) + sqrt(2)'
testcase '1.5707963267948966192313216916397514420985846996875529104874722961539082' -d70 -- 'atan2(1, sin(pi))'
testcase '-1.5707963267948966192313216916397514420985846996875529104874722961539082' -d70 -- 'atan2(-1, sin(pi))'
testcase '0.41421356237309504880' -d20 -- 'W(-1/e) + sqrt(2)'
testcase '0.41421356237309504880' -d20 -- 'Wn(-1/e) + sqrt(2)'
testcase '1.41421356237309504880' -d20 -- 'pow(sin(pi), pi) + sqrt(2)'
testcase '1.41421356237309504880' -d20 -- 'pow(sin(pi), 1/pi) + sqrt(2)'

# Regression tests added after the base output and rounding rewrite.
# Some of these failed in the previous version of the code and now
# pass; others passed originally and pass again now, but failed part
# way through the rewrite so I kept them on the grounds that they
# exercise interesting pieces of the new code.
testcase '3.14159265358979323846264338327950288419716940' -d44 --rn pi
testcase '0x1p-16495' --printf=%a ieee:00000000000000000000000000000000.8
testcase '0.12345' -d5 --rn 'sin(asin(0.12345))'
testcase '0.1235' -d4 --rn 'sin(asin(0.12345)) + pi*1e-100'
testcase '0.1234' -d4 --rn 'sin(asin(0.12345)) - pi*1e-100'
testcase '11.0010010000111111011010101000100010000101' -b2 -d40 pi
testcase '10.0102110122220102110021111102212222201112' -b3 -d40 pi
testcase '3.0210033312222020201122030020310301030121' -b4 -d40 pi
testcase '3.0663651432036134110263402244652226643520' -b7 -d40 pi
testcase '3.243f6a8885a308d313198a2e03707344a4093822' -b16 -d40 pi
testcase '3.53I5AB8P5FSA5JHK72I8ASC47WWZLACLJJ9ZN98L' -B36 -d40 pi
testcase '5.0661' -b7 -d4 --rn '12341.5*7^-4'
testcase '5.0661' -b7 -d4 --rn '12342.5*7^-4'
testcase '5.0663' -b7 -d4 --rn '12343.5*7^-4'
testcase '5.0663' -b7 -d4 --rn '12344.5*7^-4'
testcase '5.0665' -b7 -d4 --rn '12345.5*7^-4'
testcase '5.0665' -b7 -d4 --rn '12346.5*7^-4'
testcase '5.1000' -b7 -d4 --rn '12347.5*7^-4'
testcase '5.1000' -b7 -d4 --rn '12348.5*7^-4'
testcase '5.1002' -b7 -d4 --rn '12349.5*7^-4'
testcase '5.1002' -b7 -d4 --rn '12350.5*7^-4'
testcase '5.1004' -b7 -d4 --rn '12351.5*7^-4'
testcase '5.1004' -b7 -d4 --rn '12352.5*7^-4'
testcase '13.3050' -b6 -d4 --rn '12341.5*6^-4'
testcase '13.3050' -b6 -d4 --rn '12342.5*6^-4'
testcase '13.3052' -b6 -d4 --rn '12343.5*6^-4'
testcase '13.3052' -b6 -d4 --rn '12344.5*6^-4'
testcase '13.3054' -b6 -d4 --rn '12345.5*6^-4'
testcase '13.3054' -b6 -d4 --rn '12346.5*6^-4'
testcase '13.3100' -b6 -d4 --rn '12347.5*6^-4'
testcase '13.3100' -b6 -d4 --rn '12348.5*6^-4'
testcase '13.3102' -b6 -d4 --rn '12349.5*6^-4'
testcase '13.3102' -b6 -d4 --rn '12350.5*6^-4'
testcase '13.3104' -b6 -d4 --rn '12351.5*6^-4'
testcase '13.3104' -b6 -d4 --rn '12352.5*6^-4'
testcase '13.3110' -b6 -d4 --rn '12353.5*6^-4'
testcase '13.3110' -b6 -d4 --rn '12354.5*6^-4'
testcase '0.4999999999999999999999999999999999999999' -d40 '1/2 - pi/1e80'
testcase '0.5000000000000000000000000000000000000000' -d40 '1/2 + pi/1e80'
testcase '0.5' -d40 '1/2'
testcase '3' -d0 pi
testcase '3.1' -d1 pi
testcase '3.14' -d2 pi
testcase '0003.141' -w4 -d3 -- 'pi'
testcase '-0003.141' -w4 -d3 -- '-pi'
testcase '0314.159' -w4 -d3 -- 'pi*1e2'
testcase '3141.592' -w4 -d3 -- 'pi*1e3'
testcase '31415.926' -w4 -d3 -- 'pi*1e4'
testcase '31415' -d0 -- 'pi*1e4'
testcase '31410' -d-1 -- 'pi*1e4'
testcase '30000' -d-4 -- 'pi*1e4'
testcase '0' -d-5 -- 'pi*1e4'
testcase '100000' -d-5 --ru -- 'pi*1e4'
testcase '1000000' -d-6 --ru -- 'pi*1e4'
testcase '0' -d-5 --rn -- 'pi*1e4'
testcase '100000' -d-5 --rn -- 'pi*2e4'
testcase '0' --printf=%g 0
testcase '0.000000e+00' --printf=%e 0
testcase '0.000000' --printf=%f 0
testcase '0.00000' --printf=%#g 0
testcase '0x0p+0' --printf=%a 0
testcase '0x0.p+0' --printf=%#a 0
testcase '0' 0
testcase '000' -w3 0
testcase '0000' -H 0
testcase '00000000' -S 0
testcase '0000000000000000' -D 0
testcase '00000000000000000000000000000000' -Q 0
testcase '3f800000' -S -d0 --rn ieee:3f7fffff.8
testcase '9999' -d0 --rn 9999.5-pi*1e-100
testcase '9999' -d0 --rno 9999.5
testcase '10000' -d0 --rn 9999.5
testcase '10000' -d0 --rno 9999.5+pi*1e-100
testcase '66666' -d0 -b7 --rn 7^5-0.5
testcase '66666' -d0 -b7 --rno 7^5-0.5-pi*1e-100
testcase '100000' -d0 -b7 --rno 7^5-0.5
testcase '100000' -d0 -b7 --rn 7^5-0.5+pi*1e-100
testcase '0x1.921fb54443p+0' --printf=%.10a pi/2
testcase '0x1.921fb54443p+1' --printf=%.10a pi
testcase '0x1.921fb54443p+2' --printf=%.10a pi*2
testcase '0x1.921fb54443p+3' --printf=%.10a pi*4
testcase '0x1.921fb54443p+4' --printf=%.10a pi*8
testcase '0x1.921fb54443p+0' --nibble --printf=%.10a pi/2
testcase '0x3.243f6a8886p+0' --nibble --printf=%.10a pi
testcase '0x6.487ed5110bp+0' --nibble --printf=%.10a pi*2
testcase '0xc.90fdaa2217p+0' --nibble --printf=%.10a pi*4
testcase '0x1.921fb54443p+4' --nibble --printf=%.10a pi*8
testcase '40400000' -S -d-22 --rd pi
testcase '40800000' -S -d-22 --ru pi
testcase '40000000' -S -d-23 --rd pi
testcase '40800000' -S -d-23 --ru pi
testcase '4008000000000000' -D -d-51 --rd pi
testcase '4010000000000000' -D -d-51 --ru pi
testcase '4000000000000000' -D -d-52 --rd pi
testcase '4010000000000000' -D -d-52 --ru pi
testcase '7f800000' -S --rn -d0 'ieee:7f7fffff.8'
testcase '7f7fffff' -S --rn -d0 'ieee:7f7fffff.8-pi/1e100'
testcase '7f7fffff' -S --rno -d0 'ieee:7f7fffff.8'
testcase '7f800000' -S --rno -d0 'ieee:7f7fffff.8+pi/1e100'
testcase '7ff0000000000000' -D --rn -d0 'ieee:7fefffffffffffff.8'
testcase '7fefffffffffffff' -D --rn -d0 'ieee:7fefffffffffffff.8-pi/1e100'
testcase '7fefffffffffffff' -D --rno -d0 'ieee:7fefffffffffffff.8'
testcase '7ff0000000000000' -D --rno -d0 'ieee:7fefffffffffffff.8+pi/1e100'
testcase '0.2' --printf=%.1f '0.25'
testcase '0.2' --printf=%.1f 'sin(asin(0.25))-pi/1e100'
testcase '0.3' --printf=%.1f 'sin(asin(0.25))+pi/1e100'
testcase '7800' -H 0x8000
testcase '7c00' -H 0x10000
testcase '7c00' -H 0x20000
testcase '7c00' -H 0x40000
testcase '7c00' -H 0x80000
testcase '7c00' -H 0x100000
testcase '7c00' -H 0x200000
testcase '7c00' -H 0x400000
testcase '7c00' -H 0x800000
testcase 'f800' -H -- -0x8000
testcase 'fc00' -H -- -0x10000
testcase 'fc00' -H -- -0x20000
testcase 'fc00' -H -- -0x40000
testcase 'fc00' -H -- -0x80000
testcase 'fc00' -H -- -0x100000
testcase 'fc00' -H -- -0x200000
testcase 'fc00' -H -- -0x400000
testcase 'fc00' -H -- -0x800000
testcase '7ffeffffffffffffffffffffffffdfff.0000000000' -Q -d40 'ieee:7ffeffffffffffffffffffffffffffff * exp(-2^-100)'
testcase '7feffffffffffff7.0000000000' -D -d40 'ieee:7fefffffffffffff * exp(-2^-50)'
testcase '7f7fffef.00008ffffc' -S -d40 'ieee:7f7fffff * exp(-2^-20)'
testcase '7bf7.04fe2b2a8e' -H -d40 'ieee:7bff * exp(-2^-8)'
testcase '7fff0000000000000000000000000000' -Q -d40 'ieee:7ffeffffffffffffffffffffffffffff * exp(+2^-100)'
testcase '7ff0000000000000' -D -d40 'ieee:7fefffffffffffff * exp(+2^-50)'
testcase '7f800000' -S -d40 'ieee:7f7fffff * exp(+2^-20)'
testcase '7c00' -H -d40 'ieee:7bff * exp(+2^-8)'
testcase '0.779601' --printf=%g 'W(1.7)'
testcase '10.1011011111' -d10 -b2 e
testcase '00000000.00000000008' -S 'ieee:10000000 * ieee:10000000'

# Tests of the scientific-notation output mode.
testcase '10^0 * 1' -sb '1'
testcase '10^0 * 1.001' -sb '1+1/1000'
testcase '10^-1 * 9.99' -sb '1-1/1000'
testcase '2^0 * 1.001' -s2 '1+1/1000'
testcase '2^-1 * 1.998' -s2 '1-1/1000'
testcase '16^0 * 1.001' -s16 '1+1/1000'
testcase '16^-1 * 15.984' -s16 '1-1/1000'
testcase '10^4 * 2.202646579' -sb -d9 'exp(10)'
testcase '10^-5 * 4.539992976' -sb -d9 'exp(-10)'
testcase '3^9 * 1.01001221' -sb -b3 -d8 'exp(10)'
testcase '3^-10 * 2.20010102' -sb -b3 -d8 'exp(-10)'

# A few small tests of -R output.
testcase '1' -R ieee:3f800000
testcase '-1' -R ieee:bf800000
testcase '-4/9' -R 4/-9
# This one isn't known-rational at the start, because the sqrt class
# doesn't check for exactly square rationals (currently), but it turns
# out rational later. So this case exercises the ConvergentsGenerator
# code path.
testcase '4' -R 'sqrt(16)'

# Tests of tentative output. These use the undocumented
# --tentative-test option, which causes spigot to print its tentative
# output after a ! and terminate once the digit count exceeds the
# argument to the option. So we can test that we're generating the
# _right_ tentative output, or any at all in a particular tricky case.
testcase '0.1234!5 (10^-50)' --tentative-test=50 'sin(asin(0.12345))'
testcase '!0 (10^-50)' --tentative-test=50 'sin(pi)'
# The answer below is actually _wrong_ - the real answer is not
# exactly 0.12345 - but because we asked for tentative output after
# only 50 digits, and really there are 100 zeroes before getting to
# something interesting, we test that tentative output _can_ be
# printed in cases where it is wrong and would be retracted later.
testcase '0.1234!5 (10^-50)' -d110 --tentative-test=50 'sin(asin(0.12345)) + pi*1e-100'
# And now we test the same expression with the tentative-digits limit
# raised, demonstrating that although spigot has the tentative value
# 0.12345 in mind at _some_ point in the computation, it eventually
# recognises that that's not right, and retracts it.
testcase '0.12345000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000031415926535' -d110 --tentative-test=200 'sin(asin(0.12345)) + pi*1e-100'
# Check that in cases where the output requires zeroes after the
# disputed digit, they appear.
testcase '1234!5000 (10^-50)' --tentative-test=50 'sin(asin(0.12345)) * 100000000'
testcase '3!e000000 (16^-50)' -S --tentative-test=50 'sin(asin(0.125))'
testcase '3e123!400 (16^-50)' -S --tentative-test=50 'sin(asin(ieee:3e123400))'
# Tentative output in continued fraction modes.
testcase $'0\n!2 (10^40)' --tentative-test=40 -c 'sin(pi/6)'
testcase $'0\n!4 (10^40)' --tentative-test=40 -c 'sin(pi/6)^2'
testcase $'0\n1\n!3 (10^40)' --tentative-test=40 -c 'sin(pi/3)^2'
testcase $'!1 (10^40)' --tentative-test=40 -c 'sin(pi/2)^2'
testcase $'0\n!2 (10^40)' --tentative-test=40 -c 'sin(pi/4)^2'
testcase $'3\n!(10^40)' --tentative-test=40 -c '3 + abs(sin(pi))'
testcase $'!3 (10^40)' --tentative-test=40 -c '3 - abs(sin(pi))'
testcase $'3\n!7 (10^40)' --tentative-test=40 -c '22/7 + abs(sin(pi))'
testcase $'3\n7\n!(10^40)' --tentative-test=40 -c '22/7 - abs(sin(pi))'
testcase $'3\n7\n16\n!(10^40)' --tentative-test=40 -c '355/113 + abs(sin(pi))'
testcase $'3\n7\n!16 (10^40)' --tentative-test=40 -c '355/113 - abs(sin(pi))'
testcase '0!;2 (10^40)' --tentative-test=40 -cl 'sin(pi/6)'
testcase '0!;4 (10^40)' --tentative-test=40 -cl 'sin(pi/6)^2'
testcase '0;1!,3 (10^40)' --tentative-test=40 -cl 'sin(pi/3)^2'
testcase '!1 (10^40)' --tentative-test=40 -cl 'sin(pi/2)^2'
testcase '0!;2 (10^40)' --tentative-test=40 -cl 'sin(pi/4)^2'
testcase '3! (10^40)' --tentative-test=40 -cl '3 + abs(sin(pi))'
testcase '!3 (10^40)' --tentative-test=40 -cl '3 - abs(sin(pi))'
testcase '3!;7 (10^40)' --tentative-test=40 -cl '22/7 + abs(sin(pi))'
testcase '3;7! (10^40)' --tentative-test=40 -cl '22/7 - abs(sin(pi))'
testcase '3;7,16! (10^40)' --tentative-test=40 -cl '355/113 + abs(sin(pi))'
testcase '3;7!,16 (10^40)' --tentative-test=40 -cl '355/113 - abs(sin(pi))'
testcase $'0/1\n!1/2 (10^40)' --tentative-test=40 -C 'sin(pi/6)'
testcase $'0/1\n!1/4 (10^40)' --tentative-test=40 -C 'sin(pi/6)^2'
testcase $'0/1\n1/1\n!3/4 (10^40)' --tentative-test=40 -C 'sin(pi/3)^2'
testcase $'!1/1 (10^40)' --tentative-test=40 -C 'sin(pi/2)^2'
testcase $'0/1\n!1/2 (10^40)' --tentative-test=40 -C 'sin(pi/4)^2'
testcase $'3/1\n!(10^40)' --tentative-test=40 -C '3 + abs(sin(pi))'
testcase $'!3/1 (10^40)' --tentative-test=40 -C '3 - abs(sin(pi))'
testcase $'3/1\n!22/7 (10^40)' --tentative-test=40 -C '22/7 + abs(sin(pi))'
testcase $'3/1\n22/7\n!(10^40)' --tentative-test=40 -C '22/7 - abs(sin(pi))'
testcase $'3/1\n22/7\n355/113\n!(10^40)' --tentative-test=40 -C '355/113 + abs(sin(pi))'
testcase $'3/1\n22/7\n!355/113 (10^40)' --tentative-test=40 -C '355/113 - abs(sin(pi))'
testcase '!1/2 (10^40)' --tentative-test=40 -R 'sin(pi/6)'
testcase '!1/4 (10^40)' --tentative-test=40 -R 'sin(pi/6)^2'
testcase '!3/4 (10^40)' --tentative-test=40 -R 'sin(pi/3)^2'
testcase '!1 (10^40)' --tentative-test=40 -R 'sin(pi/2)^2'
testcase '!1/2 (10^40)' --tentative-test=40 -R 'sin(pi/4)^2'
testcase '!355/113 (10^40)' --tentative-test=40 -R '355/113 + abs(sin(pi))'
testcase '!355/113 (10^40)' --tentative-test=40 -R '355/113 - abs(sin(pi))'
testcase '!1 (10^-40)' --tentative-test=40 'sinc(pi-pi)'
testcase '!1 (10^-40)' --tentative-test=40 'sincn(pi-pi)'
testcase '!0 (10^-40)' --tentative-test=40 'sincn(pi+1-pi)'

# Tests of baseNfile: and cfracfile: syntaxes, inexact and exact.
echo 0.2222222222222222222222 > base7test.txt
testcase '0.3333333333' -d10 base7file:base7test.txt
echo '1;2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2' > cfractest.txt
testcase '1.4142135623' -d10 cfracfile:cfractest.txt
echo 0.100000000000001 > base2test.txt
testcase '0.5000' exit:2 -d10 base2file:base2test.txt
testcase '0.5000305175' -d10 base2xfile:base2test.txt
echo '1;1000' > cfractest2.txt
testcase '1.00' exit:2 -d10 cfracfile:cfractest2.txt
testcase '1.001' -d10 cfracxfile:cfractest2.txt
echo -n '1;1000' > cfractest3.txt
testcase '1.001' -d10 cfracxfile:cfractest3.txt
testcase '' exit:1 --safe -d10 base10file:base7test.txt
testcase '' exit:1 --safe -d10 base10xfile:base7test.txt
testcase '' exit:1 --safe -d10 cfracfd:cfractest.txt
testcase '' exit:1 --safe -d10 base10stdin
testcase '' exit:1 --safe -d10 cfracstdin

# Tests of 'let' in expressions.
testcase '3' -d40 'let x=1 in x+2'
testcase '13.8429592019974901448229600419452604080771' -d40 'let x=pi, y=e in (x+y)/(x-y)'
testcase '2' -d40 'let x=1, y=x+1 in y'
testcase '2' -d40 'let x=1, x=x+1 in x'
testcase '10' -d40 'let x=3, y=5, f(x,y)=x in f(10,20)'
testcase '20' -d40 'let x=3, y=5, f(x,y)=y in f(10,20)'
testcase '4' -d40 'let x=3, y=5, f(x,y)=x in f(x+1,y+1)'
testcase '6' -d40 'let x=3, y=5, f(x,y)=y in f(x+1,y+1)'
testcase '13.8429592019974901448229600419452604080771' -d40 'let f(x,y)=(x+y)/(x-y) in f(pi,e)'
testcase '7.2831853071795864769252867665590057683943' -d40 'let f(x)=x+1, g(x)=f(2*x) in g(pi)'
testcase '11012' -d40 'let f(x)=x+1 in let a=f(10), f(x)=x+2, b=f(10) in 1000*a+b'
testcase '303' -d40 'let f(x)=x+1 in let f(x)=3*f(x) in f(100)'
testcase '3.7182818284590452353602874713526624977572' -d40 'let f(e)=e+1 in f(e)'
testcase '300' -d40 'let f(x)=x+1, f=3 in f(100)' # f(100) here means multiplication, not function call!
testcase '12' -d40 'let f(a,x)=let g(y)=(y+x)/a in g(100) in f(10,20)'
testcase '10004000800080005' -d40 'let f(x) = x^2+1 in f(f(f(100)))'

# Exponential and logarithmic integrals.
testcase '0.5772156649015328606065120900824024310421593359399235988057672348848677' -d70 eulergamma
testcase '2492.2289762418' -d10 'Ei(10)'
testcase '1.8951178163' -d10 'Ei(1)'
testcase '-1.6228128139' -d10 'Ei(0.1)'
testcase '-1.8229239584' -d10 'Ei(-0.1)'
testcase '-0.2193839343' -d10 'Ei(-1)'
testcase '-0.000004156968929' -d15 'Ei(-10)'
testcase '1.8229239584' -d10 'E1(0.1)'
testcase '0.2193839343' -d10 'E1(1)'
testcase '0.000004156968929' -d15 'E1(10)'
testcase '1.8229239584' -d10 'En(1,0.1)'
testcase '0.2193839343' -d10 'En(1,1)'
testcase '0.000004156968929' -d15 'En(1,10)'
testcase '0.1484955067' -d10 'En(2,1)'
testcase '0.7225450221' -d10 'En(2,0.1)'
testcase '1' -d10 'En(2,0)'
testcase '!1 (10^-40)' --tentative-test=40 -d50 'En(2,sin(pi))'
testcase '0.0704542374' -d10 'En(5,1)'
testcase '0.2190159522' -d10 'En(5,0.1)'
testcase '0.25' -d10 'En(5,0)'
testcase '0.2!5 (10^-40)' --tentative-test=40 -d50 'En(5,sin(pi))'
testcase '0.2' -d10 'En(6,0)'
testcase '0.!2 (10^-40)' --tentative-test=40 -d50 'En(6,sin(pi))'
testcase '0.1999975000' -d10 'En(6,1e-5)'
testcase '0' -d10 'li(0)'
testcase '!0 (10^-40)' --tentative-test=40 -d50 'li(sin(pi))'
testcase '-0.0001281549' -d10 'li(0.001)'
testcase '1.0451637801' -d10 'li(2)'
testcase '9.9052999776' -d10 'li(20)'
testcase '0' -d10 'Li(2)'
testcase '!0 (10^-40)' --tentative-test=40 -d50 'Li(2+sin(pi))'
testcase '-1.0451637801' -d10 'Li(0)'
testcase '-1.0451637801' -d10 'Li(sin(pi))'
testcase '5762208.3302842513' -d10 'Li(1e8)'

# Trigonometric integrals.
testcase '-0.468169978584882240403351110810' -d30 'FresnelS(-10)'
testcase '-0.648125058746010538024507851688' -d30 'FresnelS(-1.234)'
testcase '-0.000000000523598775598206592491' -d30 'FresnelS(-0.001)'
testcase '0' -d30 'FresnelS(0)'
testcase '0.000000523598774675493020221233' -d30 'FresnelS(0.01)'
testcase '0.438259147390354766076756696625' -d30 'FresnelS(1)'
testcase '0.562390079733005789072678230744' -d30 'FresnelS(5.1)'
testcase '-0.499898694205515723614151847735' -d30 'FresnelC(-10)'
testcase '-0.692133432902403221351920381407' -d30 'FresnelC(-1.234)'
testcase '-0.000999999999999753259889972794' -d30 'FresnelC(-0.001)'
testcase '0' -d30 'FresnelC(0)'
testcase '0.009999999975325989025462104314' -d30 'FresnelC(0.01)'
testcase '0.779893400376822829474206413652' -d30 'FresnelC(1)'
testcase '0.499782109976894181293579077211' -d30 'FresnelC(5.1)'
testcase '-0.583670899929623342157572409285' -d30 'UFresnelS(-10)'
testcase '-0.529969705039466704545154127767' -d30 'UFresnelS(-1.234)'
testcase '-0.000000000333333333333309523809' -d30 'UFresnelS(-0.001)'
testcase '0' -d30 'UFresnelS(0)'
testcase '0.000000333333333095238095313852' -d30 'UFresnelS(0.01)'
testcase '0.310268301723381101808152423165' -d30 'UFresnelS(1)'
testcase '0.562611566336036411136279337208' -d30 'UFresnelS(5.1)'
testcase '-0.601125184813444348131191161140' -d30 'UFresnelC(-10)'
testcase '-0.976986384379953668845617811664' -d30 'UFresnelC(-1.234)'
testcase '-0.000999999999999900000000000004' -d30 'UFresnelC(-0.001)'
testcase '0' -d30 'UFresnelC(0)'
testcase '0.009999999990000000004629629628' -d30 'UFresnelC(0.01)'
testcase '0.904524237900272081474788366832' -d30 'UFresnelC(1)'
testcase '0.700767400881106138280013523028' -d30 'UFresnelC(5.1)'
testcase '-1.658347594218874049330971879389' -d30 'Si(-10)'
testcase '-1.134254091714359356886171055513' -d30 'Si(-1.234)'
testcase '-0.000999999944444446111111082766' -d30 'Si(-0.001)'
testcase '0' -d30 'Si(0)'
testcase '0.009999944444611110827664705285' -d30 'Si(0.01)'
testcase '0.946083070367183014941353313823' -d30 'Si(1)'
testcase '1.531253204712921712461203236721' -d30 'Si(5.1)'
testcase '-3.229143921013770668562293571029' -d30 'si(-10)'
testcase '-2.705050418509255976117492747153' -d30 'si(-1.234)'
testcase '-1.571796326739341065342432774406' -d30 'si(-0.001)'
testcase '-1.570796326794896619231321691639' -d30 'si(0)'
testcase '-1.560796382350285508403656986354' -d30 'si(0.01)'
testcase '-0.624713256427713604289968377816' -d30 'si(1)'
testcase '-0.039543122081974906770118454917' -d30 'si(5.1)'
testcase '-4.027979520982392072243975614535' -d30 'Ci(0.01)'
testcase '0.337403922900968134662646203889' -d30 'Ci(1)'
testcase '-0.183476263159298893723361322846' -d30 'Ci(5.1)'
testcase '2.925257190900033917259036374719' -d30 'Cin(-10)'
testcase '0.357335883694896440220840894146' -d30 'Cin(-1.234)'
testcase '0.000000249999989583333564814811' -d30 'Cin(-0.001)'
testcase '0' -d30 'Cin(0)'
testcase '0.000024999895833564814504795249' -d30 'Cin(0.01)'
testcase '0.239811742000564725943865886193' -d30 'Cin(1)'
testcase '2.389932467791111841956661813040' -d30 'Cin(5.1)'

testcase '1.851937051982466170361053370157' -d30 'Si(pi)'
testcase '1.418151576132628450245780162299' -d30 'Si(pi*2)'
testcase '1.674761798979961265948438707462' -d30 'Si(pi*3)'
testcase '1.492161225584460055488343913762' -d30 'Si(pi*4)'
testcase '1.633964846102835213309961782908' -d30 'Si(pi*5)'
testcase '0.281140725187569551129731678518' -d30 'si(pi)'
testcase '-0.152644750662268168985541529340' -d30 'si(pi*2)'
testcase '0.103965472185064646717117015822' -d30 'si(pi*3)'
testcase '-0.078635101210436563742977777877' -d30 'si(pi*4)'
testcase '0.063168519307938594078640091268' -d30 'si(pi*5)'
testcase '-3.422733378777362789592375061797' -d30 'si(-pi)'
testcase '-2.988947902927525069477101853939' -d30 'si(-pi*2)'
testcase '-3.245558125774857885179760399102' -d30 'si(-pi*3)'
testcase '-3.062957552379356674719665605402' -d30 'si(-pi*4)'
testcase '-3.204761172897731832541283474548' -d30 'si(-pi*5)'
testcase '0.472000651439568650777606107614' -d30 'Ci(pi/2)'
testcase '-0.198407560692358042506401068142' -d30 'Ci(3*pi/2)'
testcase '0.123772275403259569610748691063' -d30 'Ci(5*pi/2)'
testcase '-0.089564025928478673491319535090' -d30 'Ci(7*pi/2)'
testcase '0.070065077309319066303471205339' -d30 'Ci(9*pi/2)'
testcase '-0.057501075399484511100509763606' -d30 'Ci(11*pi/2)'
testcase '3.141592653589793238462643383279' -d30 'Cin(sin(pi)) + pi'
testcase '0.894831469484144958801022013416' -d30 'UFresnelS(sqrt(pi))'
testcase '0.430407724669015767337892650970' -d30 'UFresnelS(sqrt(2*pi))'
testcase '0.788258965566705486697496726361' -d30 'UFresnelS(sqrt(3*pi))'
testcase '0.486247023312110655335878920337' -d30 'UFresnelS(sqrt(4*pi))'
testcase '0.752442667454683164307301913028' -d30 'UFresnelS(sqrt(5*pi))'
testcase '0.511729831595120356358288653577' -d30 'UFresnelS(sqrt(6*pi))'
testcase '0.977451424291329743058620573233' -d30 'UFresnelC(sqrt(pi/2))'
testcase '0.402384257301103419813804613186' -d30 'UFresnelC(sqrt(3*pi/2))'
testcase '0.803132272418514862882983513822' -d30 'UFresnelC(sqrt(5*pi/2))'
testcase '0.476749034202717824359201776500' -d30 'UFresnelC(sqrt(7*pi/2))'
testcase '0.759158428492692762089738477936' -d30 'UFresnelC(sqrt(9*pi/2))'
testcase '0.506665396355144388535735316687' -d30 'UFresnelC(sqrt(11*pi/2))'
testcase '0.713972214021939613629086444554' -d30 'FresnelS(sqrt(2))'
testcase '0.343415678363698242195300815958' -d30 'FresnelS(2)'
testcase '0.628939658540111772818446657589' -d30 'FresnelS(sqrt(6))'
testcase '0.387968992637084042320995325783' -d30 'FresnelS(sqrt(8))'
testcase '-0.713972214021939613629086444554' -d30 'FresnelS(-sqrt(2))'
testcase '-0.343415678363698242195300815958' -d30 'FresnelS(-2)'
testcase '-0.628939658540111772818446657589' -d30 'FresnelS(-sqrt(6))'
testcase '-0.387968992637084042320995325783' -d30 'FresnelS(-sqrt(8))'
testcase '0.779893400376822829474206413652' -d30 'FresnelC(1)'
testcase '0.321056186410678069571149251917' -d30 'FresnelC(sqrt(3))'
testcase '0.640806840445253944938767034088' -d30 'FresnelC(sqrt(5))'
testcase '0.380390693768025745103558897763' -d30 'FresnelC(sqrt(7))'
testcase '-0.779893400376822829474206413652' -d30 'FresnelC(-1)'
testcase '-0.321056186410678069571149251917' -d30 'FresnelC(-sqrt(3))'
testcase '-0.640806840445253944938767034088' -d30 'FresnelC(-sqrt(5))'
testcase '-0.380390693768025745103558897763' -d30 'FresnelC(-sqrt(7))'

# Zeta function.
testcase '!-1/12 (10^40)' --tentative-test=40 -R 'zeta(-1)'
testcase '1.6449340668482264364724151666460251892189499012067984377355582293700074' -d70 'zeta(2)' # = pi^2/6
testcase '1.2020569031595942853997381615114499907649862923404988817922715553418382' -d70 'zeta(3)' # also apery
testcase '1.0823232337111381915160036965411679027747509519187269076829762154441206' -d70 'zeta(4)' # = pi^4/90
testcase '1.1762417383825827588721504519380520911697389900216558349605083462304087' -d70 'zeta(pi)'
testcase '1.2690096043357171157655698666008611088564044625771904883358159287081652' -d70 'zeta(e)'
testcase '3.6009377504588624212922075784754112775567733003375252609915700424205701' -d70 'zeta(4/3)'
testcase '3;1,1,1,1,42,18,10,1,3,1,4,1,3,5,1,1,1,1,4,1,1,1,58,1,6,1,3,4,3,1,2,1,1,3,1,1,3,2,3,15,1,2,4,1,14,1,2,2,2,3,2,1,1,4,1,1,5,1,5,1,7,2,4,3,1,4,1,73,1,1' -cl -d70 'zeta(4/3)'
testcase '-0.5009199427132187018136921122104145232842867514468597245378462629827225' -d70 'zeta(0.001)'
testcase '-0.4990820636452369673701648425135672589037487050379100968862651878869631' -d70 'zeta(-0.001)'
testcase '0.0027294997846250077824481321855946155880371532800320500969877557135385' -d70 'zeta(-2.1)'
testcase '0.0000304155891649401236768455450144189246489490032139472039049363972505' -d70 'zeta(-2.001)'
testcase '10^-14 * 3.0448457058' -sb -d10 'zeta(-2.000000000001)'
# Approximations to zeta^{-1}(2), from https://oeis.org/A107311 . We
# don't need to generate many actual output digits from this, because
# spigot will have to look all the way to the interesting part anyway
# before it can decide whether the integer part is a 2 or a 1.
testcase '2.0000000000' -d10 'zeta(1.72864723899818361813510301029769146423410984933503573232128590842317859653571008677274608108898264401)'
testcase '1.9999999999' -d10 'zeta(1.72864723899818361813510301029769146423410984933503573232128590842317859653571008677274608108898264402)'

# Hypergeometric-based functions and constants.
testcase '1.4567910310469068691864323832650819749738639432213055907941723832679264' -d70 'agm(1,2)'
testcase '2.9135820620938137383728647665301639499477278864426111815883447665358529' -d70 'agm(2,4)'
testcase '2.4746804362363044626066596035914014892516740940667358668261827406304858' -d70 'agm(2,3)'
testcase '12177.4521865389044904163545846010005172769479778075939508559772542373575332' -d70 'agm(1,100000)'
testcase '0' 'agm(0,1)'
testcase '0' 'agm(10000, 0)'
testcase '!0 (10^-40)' --tentative-test=40 'agm(pi-pi, e-e)'
testcase '!0 (10^-4)' --tentative-test=4 'agm(pi-pi, 1)'
testcase '0.8346268416740731862814297327990468089939930134903470024498273701036819' -d70 'gauss'
testcase '1.6449340668482264364724151666460251892189499012067984377355582293700074' -d70 'Li2(1)'
testcase '1.6449340668482264364724151666460251892189499012067984377355582293700074' -d70 'Li2(1+pi-pi)'
testcase '1.5886254480763753270312294739805524679449597311421238902781734494703467' -d70 'Li2(0.99)'
testcase '1.2997147230049587251710604941929533990505622836969604793735616220962498' -d70 'Li2(0.9)'
testcase '0.5822405264650125059026563201596801087441984748061264254343470478731710' -d70 'Li2(0.5)'
testcase '0.1026177910993911311138373690572322137056899394192682995315312226617174' -d70 'Li2(0.1)'
testcase '0' -d70 'Li2(0)'
testcase '-0.0976052352293215838411033418507968239816984913824186517586432796790304' -d70 'Li2(-0.1)'
testcase '-0.3658325775124496279907642196530331142879753996828027617660261592563032' -d70 'Li2(-0.4)'
testcase '-0.5281071740446665365986724070900229629986540831952700311149283592139974' -d70 'Li2(-0.6)'
testcase '-0.7521631792172616203726927134268144689605128224147463636949952951952527' -d70 'Li2(-0.9)'
testcase '-0.8224670334241132182362075833230125946094749506033992188677791146850037' -d70 'Li2(-1)'
testcase '-4.1982778868581038579121450190922372110359764493907846566556147767530649' -d70 'Li2(-10)'
testcase '-12.2387551773149389217310354588666547237047502535131106750147281699713862' -d70 'Li2(-100)'
testcase '-0.2217954820317229403940921536754886357316796682812505935054290955928612' -d70 'BesselJ(0,-9.7)'
testcase '0.7196220185275110159749158515381194000715218178569612887965322501651835' -d70 'BesselJ(0,-1.1)'
testcase '0.7651976865579665514497175261026632209092742897553252418615475491192789' -d70 'BesselJ(0,-1)'
testcase '0.9776262465382960875697461997223986721092287549406957999616797974806892' -d70 'BesselJ(0,-0.3)'
testcase '0.9999997500000156249995659722290039061821831601931724995262048055405399' -d70 'BesselJ(0,-0.001)'
testcase '1' -d70 'BesselJ(0,0)'
testcase '0.9999000024999722223958326388908178972978141632734591111714684866676168' -d70 'BesselJ(0,0.02)'
testcase '0.8812008886074052808388016628794589580426870395479847253956123346167960' -d70 'BesselJ(0,0.7)'
testcase '0.7651976865579665514497175261026632209092742897553252418615475491192789' -d70 'BesselJ(0,1)'
testcase '0.2069261023770678109966475038240717826603236286369586142839026196570363' -d70 'BesselJ(0,13)'
testcase '-0.0946983160491887620844676858261949227416244896452143035117282225553394' -d70 'BesselJ(0,67)'
testcase '-0.1166386479002129980452775891200478018571916024352535187453047196220055' -d70 'BesselJ(1,-9.7)'
testcase '-0.4709023948662929368488688365158444350642157338685983880797730406316270' -d70 'BesselJ(1,-1.1)'
testcase '-0.4400505857449335159596822037189149131273723019927652511367581717801382' -d70 'BesselJ(1,-1)'
testcase '-0.1483188162731040077414087901869686400614493324919455317072253983240183' -d70 'BesselJ(1,-0.3)'
testcase '-0.0004999999375000026041666124131951226128415708188993800462839074210923' -d70 'BesselJ(1,-0.001)'
testcase '0' -d70 'BesselJ(1,0)'
testcase '0.0099995000083332638892361099537064594307051592659878987958202568232957' -d70 'BesselJ(1,0.02)'
testcase '0.3289957415400589478487920969653696140465028237423016038758987812726535' -d70 'BesselJ(1,0.7)'
testcase '0.4400505857449335159596822037189149131273723019927652511367581717801382' -d70 'BesselJ(1,1)'
testcase '-0.0703180521217783711567693989080340450712955140557668079802993906329422' -d70 'BesselJ(1,13)'
testcase '-0.0238103831491000316376623159068599281196133180186711690876277090149987' -d70 'BesselJ(1,67)'
testcase '0.2458446877843441771044586668961170484857397924947049272673475944839963' -d70 'BesselJ(2,-9.7)'
testcase '0.1365641539566579601139365784906886636815976982677630531666914600741383' -d70 'BesselJ(2,-1.1)'
testcase '0.1149034849319004804696468813351666053454703142302052604119687944409975' -d70 'BesselJ(2,-1)'
testcase '0.0111658619490639640396457348573922616337667950056077447531561913460994' -d70 'BesselJ(2,-0.3)'
testcase '0.0000001249999895833336588541612413195009584776055875930416100366442418' -d70 'BesselJ(2,-0.001)'
testcase '0' -d70 'BesselJ(2,0)'
testcase '0.0000499983333541665277783564798280457727017633253307684105571956619627' -d70 'BesselJ(2,0.02)'
testcase '0.0587869443641917130148900427358827963758924568585912856783841833050712' -d70 'BesselJ(2,0.7)'
testcase '0.1149034849319004804696468813351666053454703142302052604119687944409975' -d70 'BesselJ(2,1)'
testcase '-0.2177442642419567911746120267330000972866767846455381232039486797544120' -d70 'BesselJ(2,13)'
testcase '0.0939875583432454775579703032618110442902927488088360596583662013907126' -d70 'BesselJ(2,67)'
testcase '0.0152593952056380796516863862762881942342061209941380848206252992162338' -d70 'BesselJ(3,-9.7)'
testcase '-0.0256945286124632817472641761775688874143213507414490779809231778197850' -d70 'BesselJ(3,-1.1)'
testcase '-0.0195633539826684059189053216217515082545089549280557905111170059838519' -d70 'BesselJ(3,-1)'
testcase '-0.0005593430477488461205343412449281817221079342494910650015238196239742' -d70 'BesselJ(3,-0.3)'
testcase '-0.0000000000208333320312500325520828812210688515314727863938626695464677' -d70 'BesselJ(3,-0.001)'
testcase '0' -d70 'BesselJ(3,0)'
testcase '0.0000001666625000416664351860119026951096475058001657833156188755692465' -d70 'BesselJ(3,0.02)'
testcase '0.0069296548267508408077224329539606509585969297353628857148679804706108' -d70 'BesselJ(3,0.7)'
testcase '0.0195633539826684059189053216217515082545089549280557905111170059838519' -d70 'BesselJ(3,1)'
testcase '0.0033198169704070507953503137594186305215488110879089239175459507085077' -d70 'BesselJ(3,13)'
testcase '0.0294215806621296123873918862508486471817203477983031726493212135756382' -d70 'BesselJ(3,67)'
testcase '0.5651591039924850272076960276098633073288996216210920094802944894792556' -d70 'BesselI(1,1)'
testcase '0.2578943053908963163624796595232096341877431496407945727309451908705658' -d70 'BesselI(1,1/2)'
testcase '0.0000000007174527655095594904475619074931214800529162958071642705397375' -d70 'BesselI(10,1.1)'
testcase '15925.3672190231702609705724304824857304147242687722981432024613232655535502' -d70 'BesselI(-2,12)'
testcase '0.6977746579640079820067905925517525994866582629980212323686300828165308' -d70 'BesselI(1,2)/BesselI(0,2)' # see http://oeis.org/A052119
testcase '0;1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20' -cl -d20 'BesselI(1,2)/BesselI(0,2)' # ... and its particularly simple continued fraction

# A test of the underlying Hg function itself, via the function F
# described on https://en.wikipedia.org/wiki/Chebyshev_equation. This
# is a solution to the Chebyshev differential equation; it's also
# expressible as a hypergeometric series via the formula given at
# http://mathworld.wolfram.com/ChebyshevPolynomialoftheFirstKind.html;
# and, perhaps more usefully, it is equal to cos(n acos x).
testcase '0.9905439982919765858698411315393436759461215611416260561133698411633139' -d70 'let F(n,x) = Hg(n,-n; 1/2; (1-x)/2) in F(1/3,catalan)'
testcase '0.9905439982919765858698411315393436759461215611416260561133698411633139' -d70 'let F(n,x) = cos(n*acos(x)) in F(1/3,catalan)'
# Test what happens when Hg is evaluated at a turning point of each sense.
testcase '0.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 'let Cos(x) = Hg(; 1/2; -x^2/4) in Cos(pi) + sqrt(2)'
testcase '2.4142135623730950488016887242096980785696718753769480731766797379907324' -d70 'let Cos(x) = Hg(; 1/2; -x^2/4) in Cos(tau) + sqrt(2)'

if ! $HAVE_FDS; then
    echo ---- SKIPPING fd tests
else
    echo ---- fd tests
    # Continued fraction of Champernowne's constant
    testcase '0;8,9,1,149083,1,1,1,4,1,1,1,3,4,1,1,1,15,4575401113910310764836466282429561185996039397104575550006620043930902626592563149379532077471286563138641209375503552094607183089984575801469863148833592141783010987,6,1,1,21,1,9,1,1,2,3,1,7,2,1,83,1,156,4,58,8,54' -cl -d39 base10fd:0 < <(perl -e 'print "0."; for ($i=1;;$i++) { print "$i" or exit 0 }')
    # Sine of Champernowne's constant. This is more interesting,
    # because the internal spigot for base10fd:0 is evaluated multiple
    # times, so this tests the FileReader's ability to remember what's
    # already come down its pipe and output it again for further
    # clients.
    testcase '0.12314341528265342194575406793995557881004957708147111362905086215948340176187120702779950739421270540745425462726799197837200042174849376893947958414813033644993043499462565832476998778044852939140517' -d200 'sin(base10fd:0)' < <(perl -e 'print "0."; for ($i=1;;$i++) { print "$i" or exit 0 }')
    # Sinc function of Champernowne's constant. This time we
    # _explicitly_ mention the same fd twice in the expression, rather
    # than constructing the two FileReaders by spigot cloning.
    testcase '0.9974616720494645805173311707960024456968929015358390562540783832156712' -d70 'sin(base10fd:0)/base10fd:0' < <(perl -e 'print "0."; for ($i=1;;$i++) { print "$i" or exit 0 }')
    # What MathWorld calls the 'Continued Fraction Constant'.
    testcase '0.69777465796400798200679059255175259948665826299802123236863008281653085276464111299696565418267656872398282187739641339311319229611953258394826715402336857207708468793165325967680260969934477352791348' -d200 cfracfd:0 < <(perl -e 'print "0;"; for ($i=1;;$i++) { print "$i," or exit 0 }')
    # And check that --safe disallows fds.
    testcase '' exit:1 --safe -d10 base10fd:0
    testcase '' exit:1 --safe -d10 cfracfd:0
fi

cd /
rm "$tmpdir"/*
rmdir "$tmpdir"

echo ---- results
echo "pass $pass"
echo "fail $fail"
echo "total $((pass+fail))"
test $fail -eq 0
