/* -*- c++ -*-
 * rmode.h: define an enumeration of rounding modes.
 */

#define RoundingMode_LIST(X)                                            \
    /*                                                                  \
     * The simplest two directed rounding modes.                        \
     */                                                                 \
    X(ROUND_TOWARD_ZERO)                                                \
    X(ROUND_AWAY_FROM_ZERO)                                             \
    /*                                                                  \
     * Four forms of round-to-nearest, differing in what they           \
     * do to exact ties. Again, the two directed tie-breaking           \
     * policies are signless.                                           \
     */                                                                 \
    X(ROUND_TO_NEAREST_EVEN)                                            \
    X(ROUND_TO_NEAREST_ODD)                                             \
    X(ROUND_TO_NEAREST_TOWARD_ZERO)                                     \
    X(ROUND_TO_NEAREST_AWAY_FROM_ZERO)                                  \
    /*                                                                  \
     * Modes which depend on the sign. Inside baseout.cpp, these are    \
     * translated into one of the modes above once the sign of the      \
     * number is known.                                                 \
     */                                                                 \
    X(ROUND_UP)                                                         \
    X(ROUND_DOWN)                                                       \
    X(ROUND_TO_NEAREST_UP)                                              \
    X(ROUND_TO_NEAREST_DOWN)                                            \
    /* end of list */

#define RoundingMode_ABBR_LIST(X)               \
    X(rz, ROUND_TOWARD_ZERO)                    \
    X(ri, ROUND_AWAY_FROM_ZERO)                 \
    X(ra, ROUND_AWAY_FROM_ZERO)                 \
    X(rn, ROUND_TO_NEAREST_EVEN)                \
    X(rne, ROUND_TO_NEAREST_EVEN)               \
    X(rno, ROUND_TO_NEAREST_ODD)                \
    X(rnz, ROUND_TO_NEAREST_TOWARD_ZERO)        \
    X(rni, ROUND_TO_NEAREST_AWAY_FROM_ZERO)     \
    X(rna, ROUND_TO_NEAREST_AWAY_FROM_ZERO)     \
    X(ru, ROUND_UP)                             \
    X(rp, ROUND_UP)                             \
    X(rd, ROUND_DOWN)                           \
    X(rm, ROUND_DOWN)                           \
    X(rnu, ROUND_TO_NEAREST_UP)                 \
    X(rnp, ROUND_TO_NEAREST_UP)                 \
    X(rnd, ROUND_TO_NEAREST_DOWN)               \
    X(rnm, ROUND_TO_NEAREST_DOWN)               \
    /* end of list */

#define RoundingMode_ENUM_DECL(val) val,
enum RoundingMode : int { RoundingMode_LIST(RoundingMode_ENUM_DECL) };
