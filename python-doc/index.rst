.. spigot documentation master file, created by
   sphinx-quickstart on Sat Oct 21 15:15:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python bindings for the exact real calculator ``spigot``
========================================================

.. toctree::

   intro
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
