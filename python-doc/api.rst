spigot module API
-----------------

.. automodule:: spigot

The ``spigot`` module's API is centred around the class
:class:`spigot.Spigot` which describes a real number. There are a few
minor supporting classes, and a lot of mathematical functions.

The Spigot data type
~~~~~~~~~~~~~~~~~~~~

This section documents the main ``Spigot`` class provided by the
``spigot`` module, and its instance methods.

.. autoclass:: spigot.Spigot

    .. automethod:: to_int(rmode=spigot.ROUND_TO_NEAREST_EVEN)

    .. automethod:: floor_int

    .. automethod:: ceil_int

    .. automethod:: sign_int

    .. automethod:: known_rational_value

    .. automethod:: base_format(base=10, uppercase=False, digitlimit=None, rmode=spigot.ROUND_TOWARD_ZERO, minintdigits=1))

    .. automethod:: base_format_str(base=10, uppercase=False, digitlimit=<non-negative integer>, rmode=spigot.ROUND_TOWARD_ZERO, minintdigits=1))

    .. method:: __format__(str)

        The ``Spigot`` class is integrated into the standard
        Python string-formatting system invoked by
        :func:`format()` or the ``format`` method on strings. So
        you can include ``Spigot`` objects in ordinary formatting
        calls:

        .. doctest::

            >>> "pi = {}".format(spigot.pi)
            'pi = 3.1415926535897932385'

        You can also specify details of the formatting in much the
        same way that you can with ``float`` objects:

        .. doctest::

            >>> "pi = {:+.7e}".format(spigot.pi)
            'pi = +3.1415927e+00'

        Beware, however, that this integration does *not* work
        with the old-style formatting system using the ``%``
        operator. If you try to format a ``Spigot`` object using a
        float-style formatting directive in that system, the
        object will be converted into an ordinary Python ``float``
        before ``Spigot`` can do anything to stop it, so you'll
        get output that looks superficially right but has lost all
        its precision:

        .. doctest::

            >>> "pi = %.30f" % spigot.pi
            'pi = 3.141592653589793115997963468544'
            >>> "pi = {:.30f}".format(spigot.pi)
            'pi = 3.141592653589793238462643383280'

        (To save you the trouble of checking: the second of those
        30-digit strings is the right value of :math:`\pi`.)

        The format string specification language for ``Spigot``
        objects is mostly the same as that for the built-in Python
        ``float`` type. The full list of elements that may appear
        in a format string, in order, is as follows:

         * alignment specification via one of the characters
           ``'<','^','=','>'``, optionally preceded by a fill
           character
         * ``'+'`` to force printing a sign even for positive
           numbers, or ``' '`` to print a space in place of a plus
           sign
         * ``'#'`` to force all digits of the fractional part to
           be output even if there are trailing zeroes
         * ``'0'`` to pad the left side of the integer part with
           zeroes (equivalent to beginning with ``'0='``)
         * a decimal number giving a minimum number of characters
           to output (by padding with the fill character if
           necessary)
         * ``','`` to print a comma every three digits of the
           number's integer part
         * ``'.'`` followed by a decimal number giving the number
           of digits of precision to display
         * a rounding-mode specification beginning with ``'r'``
           (see below)
         * a format-type specifier selected from
           ``'e','E','f','F','g','G'``.

        Most of the above elements are also supported by ``float``
        (although the ``'#'`` flag is new in Python 3), so they're
        documented in the main Python manual and I won't describe
        them again here in full detail.

        The exception is the rounding-mode specification, which is
        ``spigot``\ 's own extension to the format. The available
        rounding-mode specifications are all two- or three-letter
        abbreviations, each of which is the lower-case equivalent of
        one of the short names of the rounding-mode constants
        described in detail in :ref:`roundingmodes`.

         ======================  ==========================================
         Abbreviation            Rounding mode constant
         ======================  ==========================================
         ``'rne'``               ``spigot.ROUND_TO_NEAREST_EVEN``
         ``'rno'``               ``spigot.ROUND_TO_NEAREST_ODD``
         ``'rnu'`` or ``'rnp'``  ``spigot.ROUND_TO_NEAREST_UP``
         ``'rnd'`` or ``'rnm'``  ``spigot.ROUND_TO_NEAREST_DOWN``
         ``'rnz'``               ``spigot.ROUND_TO_NEAREST_TOWARD_ZERO``
         ``'rna'`` or ``'rni'``  ``spigot.ROUND_TO_NEAREST_AWAY_FROM_ZERO``
         ``'ru'`` or ``'rp'``    ``spigot.ROUND_UP``
         ``'rd'`` or ``'rm'``    ``spigot.ROUND_DOWN``
         ``'rz'``                ``spigot.ROUND_TOWARD_ZERO``
         ``'ra'`` or ``'ri'``    ``spigot.ROUND_AWAY_FROM_ZERO``
         ======================  ==========================================

    .. automethod:: to_digits

    .. automethod:: to_ieee_d

    .. automethod:: to_ieee_s

    .. automethod:: to_ieee_f

    .. automethod:: to_ieee_q

    .. automethod:: to_ieee_h

    .. automethod:: to_cfrac

    .. automethod:: to_convergents

Construction functions
~~~~~~~~~~~~~~~~~~~~~~

This section lists functions which construct a :class:`spigot.Spigot`
instance from various data sources.

Each of them is a function defined in the ``spigot`` module, but is
also available as a class method of the ``Spigot`` class.

(Partly, that's so that if anyone finds a reason to want to subclass
``Spigot``, these construction methods on their subclass will
automatically create instances of the subclass for them. Also, it
means you can access these functions conveniently even if you've
written something like ``from spigot import Spigot, foo, bar`` rather
than importing the whole module.)

.. autofunction:: spigot.fraction

.. automethod:: spigot.Spigot.fraction

.. autofunction:: spigot.eval

.. automethod:: spigot.Spigot.eval

.. autofunction:: spigot.from_cfrac

.. automethod:: spigot.Spigot.from_cfrac

.. autofunction:: spigot.from_digits

.. automethod:: spigot.Spigot.from_digits

.. autofunction:: spigot.from_file

.. automethod:: spigot.Spigot.from_file

.. autofunction:: spigot.from_cfrac_file

.. automethod:: spigot.Spigot.from_cfrac_file

.. _mathsfunctions:

Mathematical functions
~~~~~~~~~~~~~~~~~~~~~~

This section lists mathematical functions provided in the ``spigot``
module. Each of these is equivalent to the function of the same name
defined in the expression language accepted by :func:`spigot.eval` and
by CLI spigot.

All of these functions expect their parameters to have type
:class:`spigot.Spigot` or something that the ``Spigot`` constructor
can convert into one. All of them return a ``Spigot`` object (even the
ones where the returned value is always an integer, such as
:func:`spigot.floor`).

.. autofunction:: spigot.sqrt

.. autofunction:: spigot.cbrt

.. autofunction:: spigot.hypot(a, b, ...)

.. autofunction:: spigot.sin

.. autofunction:: spigot.cos

.. autofunction:: spigot.tan

.. autofunction:: spigot.sind

.. autofunction:: spigot.cosd

.. autofunction:: spigot.tand

.. autofunction:: spigot.asin

.. autofunction:: spigot.acos

.. autofunction:: spigot.atan

.. autofunction:: spigot.atan2

.. autofunction:: spigot.asind

.. autofunction:: spigot.acosd

.. autofunction:: spigot.atand

.. autofunction:: spigot.atan2d

.. autofunction:: spigot.sinc

.. autofunction:: spigot.sincn

.. autofunction:: spigot.exp

.. autofunction:: spigot.exp10

.. autofunction:: spigot.exp2

.. autofunction:: spigot.log(x, base=e)

.. autofunction:: spigot.log10

.. autofunction:: spigot.log2

.. autofunction:: spigot.expm1

.. autofunction:: spigot.log1p

.. autofunction:: spigot.pow

.. autofunction:: spigot.sinh

.. autofunction:: spigot.cosh

.. autofunction:: spigot.tanh

.. autofunction:: spigot.asinh

.. autofunction:: spigot.acosh

.. autofunction:: spigot.atanh

.. autofunction:: spigot.gamma

.. autofunction:: spigot.tgamma

.. autofunction:: spigot.lgamma

.. autofunction:: spigot.factorial

.. autofunction:: spigot.erf

.. autofunction:: spigot.erfc

.. autofunction:: spigot.Phi

.. autofunction:: spigot.norm

.. autofunction:: spigot.erfinv

.. autofunction:: spigot.inverf

.. autofunction:: spigot.erfcinv

.. autofunction:: spigot.inverfc

.. autofunction:: spigot.Phiinv

.. autofunction:: spigot.norminv

.. autofunction:: spigot.invPhi

.. autofunction:: spigot.invnorm

.. autofunction:: spigot.probit

.. autofunction:: spigot.W

.. autofunction:: spigot.Wn

.. autofunction:: spigot.Ei

.. autofunction:: spigot.En

.. autofunction:: spigot.E1

.. autofunction:: spigot.Ein

.. autofunction:: spigot.li

.. autofunction:: spigot.Li

.. autofunction:: spigot.Li2

.. autofunction:: spigot.Si

.. autofunction:: spigot.si

.. autofunction:: spigot.Ci

.. autofunction:: spigot.Cin

.. autofunction:: spigot.FresnelS

.. autofunction:: spigot.FresnelC

.. autofunction:: spigot.UFresnelS

.. autofunction:: spigot.UFresnelC

.. autofunction:: spigot.BesselJ

.. autofunction:: spigot.BesselI

.. autofunction:: spigot.zeta

.. autofunction:: spigot.agm

.. autofunction:: spigot.Hg

.. autofunction:: spigot.abs

.. autofunction:: spigot.sign

.. autofunction:: spigot.ceil

.. autofunction:: spigot.floor

.. autofunction:: spigot.frac

.. autofunction:: spigot.fmod

.. autofunction:: spigot.algebraic(lo, hi, a0, a1, ..., an)

.. function:: spigot.round_rz(x)
              spigot.round_ri(x)
              spigot.round_ra(x)
              spigot.round_rn(x)
              spigot.round_rne(x)
              spigot.round_rno(x)
              spigot.round_rnz(x)
              spigot.round_rni(x)
              spigot.round_rna(x)
              spigot.round_ru(x)
              spigot.round_rp(x)
              spigot.round_rd(x)
              spigot.round_rm(x)
              spigot.round_rnu(x)
              spigot.round_rnp(x)
              spigot.round_rnd(x)
              spigot.round_rnm(x)
              spigot.round(x, rmode=spigot.ROUND_TO_NEAREST_EVEN)

This family of functions rounds the input number :math:`x` to an
integer value. They return the resulting integer in the form of an
instance of the :class:`spigot.Spigot` class, not an ordinary Python
integer.

The rounding can be performed according to any of the rounding modes
described in :ref:`roundingmodes`. Each function with a suffix on its
name behaves like the rounding mode described by that suffix. For
example, :func:`spigot.round_rne` behaves in accordance with the
:data:`spigot.RNE` or :data:`spigot.ROUND_TO_NEAREST_EVEN` rounding
mode.

The function :func:`spigot.round` itself takes the rounding mode as an
additional argument.

.. function:: spigot.fracpart_rz(x)
              spigot.fracpart_ri(x)
              spigot.fracpart_ra(x)
              spigot.fracpart_rn(x)
              spigot.fracpart_rne(x)
              spigot.fracpart_rno(x)
              spigot.fracpart_rnz(x)
              spigot.fracpart_rni(x)
              spigot.fracpart_rna(x)
              spigot.fracpart_ru(x)
              spigot.fracpart_rp(x)
              spigot.fracpart_rd(x)
              spigot.fracpart_rm(x)
              spigot.fracpart_rnu(x)
              spigot.fracpart_rnp(x)
              spigot.fracpart_rnd(x)
              spigot.fracpart_rnm(x)
              spigot.fracpart(x, rmode=spigot.ROUND_TO_NEAREST_EVEN)

This family of functions returns the fractional part of the input
number, obtained by subtracting the integer part. Each function
calculates the integer part according to the specified rounding mode.
For example, ``spigot.fracpart_rne(x)`` is equivalent to
``x-spigot.round_rne(x)``.

The function :func:`spigot.fracpart` itself takes the rounding mode as
an additional argument.

.. function:: spigot.remainder_rz(x,y)
              spigot.remainder_ri(x,y)
              spigot.remainder_ra(x,y)
              spigot.remainder_rn(x,y)
              spigot.remainder_rne(x,y)
              spigot.remainder_rno(x,y)
              spigot.remainder_rnz(x,y)
              spigot.remainder_rni(x,y)
              spigot.remainder_rna(x,y)
              spigot.remainder_ru(x,y)
              spigot.remainder_rp(x,y)
              spigot.remainder_rd(x,y)
              spigot.remainder_rm(x,y)
              spigot.remainder_rnu(x,y)
              spigot.remainder_rnp(x,y)
              spigot.remainder_rnd(x,y)
              spigot.remainder_rnm(x,y)
              spigot.remainder(x, y, rmode=spigot.ROUND_TO_NEAREST_EVEN)

This family of functions returns the remainder from dividing :math:`x`
by :math:`y`, in the sense that they return :math:`x - qy` for some
integer quotient :math:`q`. For each function, the quotient is
obtained by rounding the true quotient :math:`x/y` to an integer
according to the specified rounding mode. For example,
``spigot.remainder_rne(x,y)`` is equivalent to
``x-y*spigot.round_rne(x/y)``.

The function :func:`spigot.remainder` itself takes the rounding mode
as an additional argument.

Mathematical constants
~~~~~~~~~~~~~~~~~~~~~~

This section lists mathematical constants provided in the ``spigot``
module, as instances of the :class:`spigot.Spigot` class. Each of
these constants is also available under the same name in the
expression language accepted by :func:`spigot.eval` and by CLI spigot.

.. data:: spigot.pi

    The mathematical constant :math:`\pi`. This is the ratio of a
    circle's radius to half its circumference, and the smallest
    positive root of the sine function. (Occasionally also known as
    'Archimedes' constant'.)

.. data:: spigot.tau

    The mathematical constant :math:`\tau`. This is the ratio of a
    circle's radius to its circumference, and the period of the sine,
    cosine and tangent functions.

    (:math:`\tau` is the same thing as :math:`2\pi`, of course, and
    :math:`\pi` is a more widely used name. On the other hand, there
    is a school of thought -- see the `Tau Manifesto
    <https://www.tauday.com/tau-manifesto>`_ -- that says :math:`\tau`
    is a more sensible choice for the circle constant in most cases.
    ``spigot`` provides both constants, so you can make your own
    choice.)

.. data:: spigot.e

    The mathematical constant :math:`e`. This is the base of natural
    logarithms; the number such that :math:`d/dx\,(e^x) = e^x`; and
    the limit as :math:`n\to\infty` of :math:`(1+\frac1n)^n`.

.. data:: spigot.phi

    The mathematical constant :math:`\phi`, otherwise known as the
    golden ratio. This is the limiting ratio of consecutive numbers in
    the Fibonacci series; the positive real root of :math:`x^2-x-1`;
    the number whose continued fraction coefficients are all 1; and
    :math:`\frac12(1+\sqrt5)`.

.. data:: spigot.apery

    Apéry's constant, otherwise known as :math:`\zeta(3)`. This is the
    sum of the reciprocals of the cubes of the positive integers.

.. data:: spigot.catalan

    Catalan's constant. This is the alternating sum of the reciprocals
    of the squares of the positive odd integers, i.e.
    :math:`\frac{1}{1^2}-\frac{1}{3^2}+\frac{1}{5^2}-\frac{1}{7^2}+\cdots`.
    It's also the value of various integrals, such as
    :math:`\int_1^\infty\frac{\log t}{1+t^2}dt`.

.. data:: spigot.eulergamma

    The Euler-Mascheroni constant :math:`\gamma`. This is defined as
    the limiting error in approximating the sum of :math:`1/n` with
    the integral of :math:`1/x`. In other words,

    .. math::

         \gamma = \lim_{n\to\infty} \left(
             \sum_{i=1}^{n-1} \frac1i - \int_1^n \frac{dx}{x} \right)

.. data:: spigot.gauss

    Gauss's constant. This is the reciprocal of the
    arithmetic-geometric mean of 1 and :math:`\sqrt2`.

.. _roundingmodes:

Rounding modes
~~~~~~~~~~~~~~

When a ``Spigot`` object is converted to a limited-precision output
format, e.g. to an integer by :func:`spigot.Spigot.to_int`, or to a
limited-length string by :func:`spigot.Spigot.base_format_str`, and
the true value of the real number is not one of the values that the
output format can represent exactly, then the result must be rounded
to one of the adjacent representable outputs -- either the next larger
one, or the next smaller one.

``spigot`` supports a range of different rules for deciding which way
to round. Each one is described by a numeric constant. This section
lists all the rounding-mode constants and their semantics.

(Don't depend on the *values* of these constants. They are values from
an internal enumeration and might change between releases. Always use
the symbolic names.)

.. data:: spigot.ROUND_TO_NEAREST_EVEN, spigot.RNE

    In this mode, the returned value is literally the *nearest*
    representable value, i.e. the one that is closest in value to
    :math:`x`, regardless of whether it is larger or smaller than
    :math:`x` itself.

    For example, the first of the examples below has been rounded up
    from :math:`31415.926\ldots`, and the second has been rounded down
    from :math:`314159.26\ldots`, because in each case rounding in the
    other direction would have changed the value by more:

    .. doctest::

      >>> (spigot.pi*10000).to_int()
      31416
      >>> (spigot.pi*100000).to_int()
      314159

    If the number is *exactly* half way between the two possible
    answers, there is a tie. The tie-breaking rule is based on the
    *parity* of the two answers: the returned value is whichever of
    the two answers is -- in some sense -- even.

    When this rounding mode is used in a context of converting to an
    integer, the meaning of 'even' is clear: one of the candidate
    output integers is always odd, and the other is always even, and
    whichever one is even, we pick that one.

    .. doctest::

      >>> (spigot.Spigot("1234.5")).to_int()   # rounds down
      1234
      >>> (spigot.Spigot("1235.5")).to_int()   # rounds up
      1236
      >>> (spigot.Spigot("1236.5")).to_int()   # rounds down
      1236
      >>> (spigot.Spigot("1237.5")).to_int()   # rounds up
      1238

    When this rounding mode is used with a non-integer output, the
    notion of the answer being 'even' needs a bit more interpretation.
    An obvious interpretation -- in fact, the one used by IEEE 754 --
    is to pick whichever answer has an even *final digit*:

    .. doctest::

      >>> spigot.Spigot("0.12345").base_format_str(
      ...     digitlimit=4, rmode=spigot.RNE)
      '0.1234'
      >>> spigot.Spigot("0.12355").base_format_str(
      ...     digitlimit=4, rmode=spigot.RNE)
      '0.1236'

    If the output number base is also even, this interpretation is
    fine. But in an odd base, it doesn't quite make sense. Suppose you
    want to convert the number :math:`\frac{13}{14}` to base 7, with
    one fractional digit. The possible output strings are ``'0.6'``
    and ``'1.0'``, representing :math:`\frac{12}{14}` and
    :math:`\frac{14}{14}` respectively, so the true input value is
    exactly half way between them. But *both* have an even last digit!

    The solution is to reinterpret the rule. Instead of looking at
    just the last digit, the way to decide whether the output counts
    as 'even' or not is to imagine discarding the point, and treating
    the whole output as if it was an integer in the same number base.
    So the output string ``'0.6'`` becomes the integer 6, and the
    output string ``'1.0'`` becomes the base-7 representation ``10``,
    i.e. the integer 7. Now we're back to the situation in which
    *exactly one* of the two outputs is even, so we can break our tie
    unambiguously.

    So we expect the tie-breaking rule in this case to return the
    smaller output ``'0.6'``. But in the similar case when you choose
    between ``'1.6'`` and ``'2.0'``, regarding the two strings as
    base-7 integers gives you 13 and 14 respectively, so in that case
    the rule should round *up* to ``'2.0'``.

    .. doctest::

        >>> spigot.fraction(13,14).base_format_str(
        ...     base=7, digitlimit=1, rmode=spigot.RNE)
        '0.6'
        >>> spigot.fraction(27,14).base_format_str(
        ...     base=7, digitlimit=1, rmode=spigot.RNE)
        '2.0'

.. data:: spigot.ROUND_TOWARD_ZERO, spigot.RZ

    In this mode, the returned value will be whichever of the two
    possible outputs is closer to zero, i.e. whichever one has the
    smaller absolute value.

    This rounding rule is sometimes called 'truncating' rather than
    'rounding', because it has the effect of chopping off the decimal
    expansion of the output number, without ever altering the last
    digit to something other than what it would be in the full
    infinite expansion.

    .. doctest::

      >>> (spigot.pi*10000).to_int(rmode=spigot.RZ)
      31415
      >>> (-spigot.pi*10000).to_int(rmode=spigot.RZ)
      -31415

.. data:: spigot.ROUND_AWAY_FROM_ZERO, spigot.RI, spigot.RA

    This is the opposite of ``ROUND_TOWARD_ZERO``: in this mode, the
    returned value will be whichever of the two possible outputs is
    *further away* from 0. So it *always* (except in the case of an
    exact value that needs no rounding) increments the last digit by 1
    compared to what it would be in the full infinite expansion.

    .. doctest::

      >>> (spigot.pi*10000).to_int(rmode=spigot.RA)
      31416
      >>> (-spigot.pi*10000).to_int(rmode=spigot.RA)
      -31416

.. data:: spigot.ROUND_DOWN, spigot.RD, spigot.RM

    In this mode, the returned value is always the smaller one,
    :math:`\lfloor x\rfloor`. That is, for :math:`x>0` this mode
    rounds towards zero, and for :math:`x<0` it rounds away from zero.

    .. doctest::

      >>> (spigot.pi*10000).to_int(rmode=spigot.RD)
      31415
      >>> (-spigot.pi*10000).to_int(rmode=spigot.RD)
      -31416

.. data:: spigot.ROUND_UP, spigot.RU, spigot.RP

    In this mode, the returned value is always the larger one,
    :math:`\lceil x\rceil`. That is, for :math:`x>0` this mode rounds
    away from zero, and for :math:`x<0` it rounds towards zero.

    .. doctest::

      >>> (spigot.pi*10000).to_int(rmode=spigot.RU)
      31416
      >>> (-spigot.pi*10000).to_int(rmode=spigot.RU)
      -31415

.. data:: spigot.ROUND_TO_NEAREST_AWAY_FROM_ZERO, spigot.RNA, spigot.RNI

    In this mode, like ``ROUND_TO_NEAREST_EVEN``, the returned value
    is the nearest one to :math:`x`, in the usual case where there is
    a unique nearest value.

    The two modes differ in their tie-breaking behaviour. In *this*
    mode, if :math:`x` is exactly half way between two representable
    outputs, the returned one will be whichever is further away from
    zero.

    .. doctest::

      >>> spigot.Spigot("1234.5").to_int(rmode=spigot.RNA)
      1235
      >>> spigot.Spigot("1235.5").to_int(rmode=spigot.RNA)
      1236
      >>> spigot.Spigot("-1234.5").to_int(rmode=spigot.RNA)
      -1235
      >>> spigot.Spigot("-1235.5").to_int(rmode=spigot.RNA)
      -1236

.. data:: spigot.ROUND_TO_NEAREST_TOWARD_ZERO, spigot.RNZ

    This mode rounds to the nearest representable value, breaking ties
    by returning whichever of the candidate outputs is closer to zero.

    .. doctest::

      >>> spigot.Spigot("1234.5").to_int(rmode=spigot.RNZ)
      1234
      >>> spigot.Spigot("1235.5").to_int(rmode=spigot.RNZ)
      1235
      >>> spigot.Spigot("-1234.5").to_int(rmode=spigot.RNZ)
      -1234
      >>> spigot.Spigot("-1235.5").to_int(rmode=spigot.RNZ)
      -1235

.. data:: spigot.ROUND_TO_NEAREST_DOWN, spigot.RND, spigot.RNM

    This mode rounds to the nearest representable value, breaking ties
    by returning whichever of the candidate outputs is smaller (in the
    sense of 'more negative', or 'closer to :math:`-\infty`).

    .. doctest::

      >>> spigot.Spigot("1234.5").to_int(rmode=spigot.RND)
      1234
      >>> spigot.Spigot("1235.5").to_int(rmode=spigot.RND)
      1235
      >>> spigot.Spigot("-1234.5").to_int(rmode=spigot.RND)
      -1235
      >>> spigot.Spigot("-1235.5").to_int(rmode=spigot.RND)
      -1236

.. data:: spigot.ROUND_TO_NEAREST_UP, spigot.RNU, spigot.RNP

    This mode rounds to the nearest representable value, breaking ties
    by returning whichever of the candidate outputs is larger (in the
    sense of 'more positive', or 'closer to :math:`+\infty`).

    .. doctest::

      >>> spigot.Spigot("1234.5").to_int(rmode=spigot.RNU)
      1235
      >>> spigot.Spigot("1235.5").to_int(rmode=spigot.RNU)
      1236
      >>> spigot.Spigot("-1234.5").to_int(rmode=spigot.RNU)
      -1234
      >>> spigot.Spigot("-1235.5").to_int(rmode=spigot.RNU)
      -1235

.. data:: spigot.ROUND_TO_NEAREST_ODD, spigot.RNO

    This mode rounds to the nearest representable value, breaking ties
    by returning whichever of the candidate outputs is *odd*.

    In other words, this tie-breaking rule is exactly the opposite of
    the default :data:`spigot.ROUND_TO_NEAREST_EVEN`, in the sense
    that it will return whichever of the two possibilities is *not*
    the one ``ROUND_TO_NEAREST_EVEN`` would have chosen.

    .. doctest::

      >>> spigot.Spigot("1234.5").to_int(rmode=spigot.RNO)
      1235
      >>> spigot.Spigot("1235.5").to_int(rmode=spigot.RNO)
      1235
      >>> spigot.Spigot("1236.5").to_int(rmode=spigot.RNO)
      1237
      >>> spigot.Spigot("1237.5").to_int(rmode=spigot.RNO)
      1237
      >>> spigot.fraction(13,14).base_format_str(
      ...     base=7, digitlimit=1, rmode=spigot.RNO)
      '1.0'
      >>> spigot.fraction(27,14).base_format_str(
      ...     base=7, digitlimit=1, rmode=spigot.RNO)
      '1.6'

Top-level aliases
~~~~~~~~~~~~~~~~~

These functions are provided in the ``spigot`` module as aliases for
instance methods of :class:`spigot.Spigot`, mostly so that they can be
invoked via a syntax that's consistent with the functions in
:ref:`mathsfunctions`.

.. autofunction:: spigot.to_int(x, rmode=spigot.ROUND_TO_NEAREST_EVEN)

.. autofunction:: spigot.floor_int

.. autofunction:: spigot.ceil_int

.. autofunction:: spigot.sign_int

Auxiliary types
~~~~~~~~~~~~~~~

This section documents the helper classes and exceptions defined by
the ``spigot`` module.

.. autoclass:: spigot.BASE_NEG

   .. data:: value

   A non-negative integer, storing the integer part of the absolute
   value of the number represented by the digit sequence started by
   this object.

.. autoclass:: spigot.BASE_DIGIT

   .. data:: base

   An integer indicating the number base in which this object's digit
   should be interpreted.

   .. data:: digit

   An integer indicating the value of the digit stored in this object.

.. autoexception:: spigot.EndOfFile
