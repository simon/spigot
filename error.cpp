/* -*- c++ -*-
 * error.cpp: trivial constructors and member functions from error.h.
 */

#include <string.h>

#include <memory>
#include <utility>

using std::make_unique;
using std::move;

#include "spigot.h"
#include "error.h"

spigot_error_base::spigot_error_base(string msg)
    : errmsg(msg)
{
}

const char *spigot_error_base::text() const
{
    return errmsg.c_str();
}

eof_event::eof_event(const char *filename)
    : spigot_error_base(filename)
{
}
