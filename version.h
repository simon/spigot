/* -*- c++ -*-
 * This header file provides the version #define for a particular
 * build of spigot.
 *
 * When my automated build system does a full build, Buildscr
 * completely overwrites this file with information appropriate to
 * that build. The information _here_ is default stuff used for local
 * development runs of 'make'.
 */

#ifndef VER /* so that you can make CPPFLAGS+='-DVER="\"version\""' */
#define VER "unidentified build"
#endif
