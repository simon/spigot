/* -*- c++ -*-
 * expr.h: header for expr.cpp.
 */

/*
 * Class implementing a function that maps variable names to spigots.
 * Optionally provide one of these to expr_parse as a variable scope.
 * Naturally, it returns nullptr if the variable name is unrecognised.
 * It's expected to be case sensitive (though, of course, it can
 * behave insensitively if it prefers).
 */
class GlobalScope {
  public:
    virtual Spigot lookup(const char *varname) = 0;
};

class FileAccessContext; // defined properly in io.h

Spigot expr_parse(const char *expr, GlobalScope *scope,
                  FileAccessContext &fac);
Spigot literal_parse(const char *expr);

extern bool enable_debug_fns;

/*
 * Allow language-binding modules to extract the list of function
 * names and arities.
 */
struct ExprFunction {
    string name;
    int arity; // < 0 means variadic
};
vector<ExprFunction> expr_functions();
